<?php

use Illuminate\Database\Seeder;

class ProdutosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produtos')->insert([
            'nome_produto' => 'Dísco Rigido 500Gb Sata',
            'quantidade_estoque' => '20',
            'codigo_barras' => '65443566' ,
            'data_compra' => '2018-03-19'
               
        ]);

        DB::table('produtos')->insert([
            'nome_produto' => 'Memória DDR3 8Gb 1333Mhz',
            'quantidade_estoque' => '7',
            'codigo_barras' => '23213123' ,
            'data_compra' => '2018-03-19'
               
        ]);


    }
}
