<?php

use Illuminate\Database\Seeder;

class UsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->insert([
            'nome' => 'Byte',
            'sobrenome' => 'Informática',
            'email' => 'contato@byteinformatica.com.br',
            'sexo' => 'M',
            'telefone' => '53981014003',
            'tipo_usuario' => 1,
            'ativo' => 1,
            'password' => bcrypt('789456123ab')
               
        ]);
    }
}
