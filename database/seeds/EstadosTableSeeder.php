<?php

use Illuminate\Database\Seeder;

class EstadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estados')->insert([
            'nome_estado' => 'Rio Grande do Sul',
            'sigla_estado' => 'RS',     
        ]);


        DB::table('estados')->insert([
            'nome_estado' => 'Santa Catarina',
            'sigla_estado' => 'SC',     
        ]);


    }
}
