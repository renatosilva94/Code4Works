<?php

use Illuminate\Database\Seeder;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->insert([
            'nome_cliente' => 'Ariel Medeiros Peres',
            'telefone' => '(53) 981462809',
            'cpf' => '040.122.730-86',
            'cep' => '96085-357',
            'bairro' => 'Areal',
            'rua' => 'Elgar Carlos Hadler 1814',
            'email' => 'ariel.peres@hotmail.com',
            'numero' => 'Bloco 9 Apartamento 201',
            'tipo_cliente' => 1,
            'ativo' => 1,
            'estado_id' => 1,
            'cidade_id' => 1     
        ]);

        DB::table('clientes')->insert([
            'nome_cliente' => 'Claudio Renato Pereira da Silva',
            'telefone' => '(53) 981014003',
            'cpf' => '020.774.010-01',
            'cep' => '96085-357',
            'bairro' => 'Areal',
            'rua' => 'Elgar Carlos Hadler 1814',
            'email' => 'renato_silva94@live.com',
            'numero' => 'Bloco 9 Apartamento 201',
            'tipo_cliente' => 1,
            'ativo' => 1,
            'estado_id' => 1,
            'cidade_id' => 1     
        ]);

        DB::table('clientes')->insert([
            'nome_fantasia' => 'A & R Doces Gourmet',
            'telefone' => '(53) 981462809',
            'cnpj' => '000.000.000-00',
            'cep' => '96085-357',
            'bairro' => 'Areal',
            'rua' => 'Elgar Carlos Hadler 1814',
            'email' => 'contato@aerdocesgourmet.com.br',
            'numero' => 'Bloco 9 Apartamento 201',
            'tipo_cliente' => 2,
            'ativo' => 1,
            'estado_id' => 1,
            'cidade_id' => 1     
        ]);

        





    }
}
