<?php

use Illuminate\Database\Seeder;

class CidadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cidades')->insert([
            'nome_cidade' => 'Pelotas',
            'estado_id' => 1,     
        ]);


        DB::table('cidades')->insert([
            'nome_cidade' => 'Florianópolis',
            'estado_id' => 2,     
        ]);
    }
}
