<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');

            /* Dados Cliente PF */
            $table->string('nome_cliente')->nullable();
            $table->string('telefone');
            $table->string('cpf')->nullable();
            $table->string('cep');
            $table->string('bairro');
            $table->string('rua');
            $table->string('numero');
            $table->string('email');

            /*Dados Cliente PJ*/
            $table->string('nome_fantasia')->nullable();
            $table->string('razao_social')->nullable();
            $table->string('cnpj')->nullable();



            
            $table->integer('tipo_cliente');
            $table->integer('ativo');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
