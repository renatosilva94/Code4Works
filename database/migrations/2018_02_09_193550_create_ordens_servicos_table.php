<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdensServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordens_servicos', function (Blueprint $table) {
            $table->increments('id');           
            $table->string('problema_relatado');
            $table->string('data');
            $table->string('hora');
            $table->double('valor_total');
            $table->string('solucao');
            $table->integer('status');
            $table->integer('tipo_cliente');
            
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
