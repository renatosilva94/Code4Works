<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('compras', function (Blueprint $table) {
            $table->increments('id');           


            $table->string('numero_nota_fiscal');
            $table->string('quantidade_embalagens');
            $table->string('preco_embalagem');
            $table->string('quantidade_produtos_embalagem');
            $table->string('preco_unitario');
            $table->double('preco_total');
            
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
