<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrcamentosProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('orcamentos_produtos', function (Blueprint $table) {

            $table->integer('orcamentos_id')->unsigned();
        
            $table->integer('produtos_id')->unsigned();
        
            $table->foreign('orcamentos_id')
                ->references('id')->on('orcamentos')
                ->onDelete('cascade');
        
            $table->foreign('produtos_id')
                ->references('id')->on('produtos')
                ->onDelete('cascade');

            $table->double('valor_total'); 
            $table->integer('quantidade'); 
            $table->double('valor_unitario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
