<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuncionariosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('funcionarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('sobrenome');
            $table->string('rg');
            $table->string('cpf');
            $table->string('cep');
            $table->string('bairro');
            $table->string('rua');
            $table->string('numero');
            $table->string('password');
            $table->string('pispasep');
            $table->string('cargo');
            $table->string('email');
            $table->integer('ativo');
            
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
