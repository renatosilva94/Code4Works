<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFornecedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('fornecedores', function (Blueprint $table) {
            $table->increments('id');

            /* Dados Cliente Fornecedor*/
            $table->string('nome_fantasia');
            $table->string('razao_social');
            $table->string('cnpj');
            $table->string('cep');
            $table->string('bairro');
            $table->string('rua');
            $table->string('numero');
            $table->string('telefone');
            $table->string('email');
            
            $table->integer('tipo_cliente');
            $table->integer('ativo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
