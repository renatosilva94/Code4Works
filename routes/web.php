<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'usuarios'], function() {


/*Página de Login*/

Route::get('/', 'EmpresaController@EmpresaLogin')->name('/');

Route::get('empresa.login', 'EmpresaController@EmpresaLogin')->name('empresa.login');

/*Página da Dashboard da Empresa*/

Route::get('empresa.dashboard.principal', 'EmpresaController@EmpresaDashboardPrincipal')->name('empresa.dashboard.principal');

/*Página de Cadastro de Clientes PF*/

Route::get('empresa.cadastro.cliente.pf', 'EmpresaController@EmpresaCadastroClientePF')->name('empresa.cadastro.cliente.pf');

/*Página de Cadastro de Clientes PF*/

Route::get('empresa.cadastro.cliente.pj', 'EmpresaController@EmpresaCadastroClientePJ')->name('empresa.cadastro.cliente.pj');

/*Página de Lista de Clientes Pessoa Fisica*/

Route::get('empresa.lista.cliente.pf', 'EmpresaController@EmpresaListaClientePF')->name('empresa.lista.cliente.pf');

/*Página de Lista de Clientes Pessoa Juridica*/

Route::get('empresa.lista.cliente.pj', 'EmpresaController@EmpresaListaClientePJ')->name('empresa.lista.cliente.pj');

/*Página de Cadastro*/

Route::get('empresa.cadastro', 'EmpresaController@EmpresaCadastro')->name('empresa.cadastro');

/*Método para Validar o Cadastro*/

Route::post('empresa.cadastro.efetuar', 'EmpresaController@EmpresaCadastroEfetuar')->name('empresa.cadastro.efetuar');

/*Método para Efetuar o Login*/

Route::post('empresa.login.efetuar', 'EmpresaController@EmpresaLoginEfetuar')->name('empresa.login.efetuar');

/*Ajax - Lista de Cidades Baseada no Estado*/

Route::get('ajax/pegar-lista-cidades','EmpresaController@ListaCidadesEstados');

/*Método para a Empresa Cadastrar o Cliente PF*/

Route::post('empresa.cadastra.cliente.pf', 'EmpresaController@EmpresaCadastraClientePF')->name('empresa.cadastra.cliente.pf');

/*Método para a Empresa Cadastrar o Cliente PJ*/

Route::post('empresa.cadastra.cliente.pj', 'EmpresaController@EmpresaCadastraClientePJ')->name('empresa.cadastra.cliente.pj');

/*Método para Buscar Cliente PF por Filtros*/

Route::post('empresa.lista.cliente.pf.filtros', 'EmpresaController@EmpresaListaClientePFFiltros')->name('empresa.lista.cliente.pf.filtros');

/*Método para Buscar Cliente PJ por Filtros*/

Route::post('empresa.lista.cliente.pj.filtros', 'EmpresaController@EmpresaListaClientePJFiltros')->name('empresa.lista.cliente.pj.filtros');

/*Página de Cadastro de Funcionários*/

Route::get('empresa.cadastro.funcionario', 'EmpresaController@EmpresaCadastroFuncionario')->name('empresa.cadastro.funcionario');

/*Método para a Empresa Cadastrar o Funcionário*/

Route::post('empresa.cadastra.funcionario', 'EmpresaController@EmpresaCadastraFuncionario')->name('empresa.cadastra.funcionario');

/*Método para a Empresa Editar o Cliente PF e Salvar a Edição*/

Route::get('empresa.edita.cliente.pf/{id}', 'EmpresaController@EmpresaEditaClientePf')->name('empresa.edita.cliente.pf');
Route::post('empresa.salva.cliente.pf.editado/{id}', 'EmpresaController@EmpresaSalvaClientePfEditado')->name('empresa.salva.cliente.pf.editado');


/*Método para a Empresa Deletar o Cliente PF*/

Route::delete('empresa.deleta.cliente.pf/{id}', 'EmpresaController@EmpresaDeletaClientePf')->name('empresa.deleta.cliente.pf');


/*Página de Lista de Funcionários*/

Route::get('empresa.lista.funcionario', 'EmpresaController@EmpresaListaFuncionario')->name('empresa.lista.funcionario');


/*Método para a Empresa Editar o Cliente PJ e Salvar a Edição*/

Route::get('empresa.edita.cliente.pj/{id}', 'EmpresaController@EmpresaEditaClientePj')->name('empresa.edita.cliente.pj');
Route::post('empresa.salva.cliente.pj.editado/{id}', 'EmpresaController@EmpresaSalvaClientePjEditado')->name('empresa.salva.cliente.pj.editado');


/*Método para a Empresa Listar os Produtos*/

Route::get('empresa.lista.produto', 'EmpresaController@EmpresaListaProduto')->name('empresa.lista.produto');


/*Página de Cadastro de Produtos*/

Route::get('empresa.cadastro.produto', 'EmpresaController@EmpresaCadastroProduto')->name('empresa.cadastro.produto');
Route::post('empresa.cadastra.produto', 'EmpresaController@EmpresaCadastraProduto')->name('empresa.cadastra.produto');


/*Método para a Empresa Editar o Funcionario e Salvar a Edição*/

Route::get('empresa.edita.funcionario/{id}', 'EmpresaController@EmpresaEditaFuncionario')->name('empresa.edita.funcionario');
Route::post('empresa.salva.funcionario.editado/{id}', 'EmpresaController@EmpresaSalvaFuncionarioEditado')->name('empresa.salva.funcionario.editado');

/*Método para a Empresa Efetuar o Logout*/

Route::get('empresa.efetuar.logout', 'EmpresaController@EmpresaEfetuarLogout')->name('empresa.efetuar.logout');

/*Método para a Empresa Listar Ordens de Serviço*/

Route::get('empresa.lista.ordem.servico.pf', 'EmpresaController@EmpresaListaOrdemServicoPF')->name('empresa.lista.ordem.servico.pf');


/*Página de Cadastro de Ordens de Serviço e Salva-las*/

Route::get('empresa.cadastro.ordem.servico.pf', 'EmpresaController@EmpresaCadastroOrdemServicoPf')->name('empresa.cadastro.ordem.servico.pf');
Route::post('empresa.cadastra.ordem.servico.pf', 'EmpresaController@EmpresaCadastraOrdemServicoPf')->name('empresa.cadastra.ordem.servico.pf');


/*Gerar PDF da Ordem de Serviço*/

Route::get('empresa.gerar.pdf.ordem.servico.pf/{id}', 'EmpresaController@EmpresaGerarPdfOrdemServicoPF')->name('empresa.gerar.pdf.ordem.servico.pf');



/*Deletar Ordem de Serviço*/

Route::delete('empresa.deleta.ordem.servico.cliente.pf/{id}', 'EmpresaController@EmpresaDeletaOrdemServicoPF')->name('empresa.deleta.ordem.servico.cliente.pf');


/*Editar Perfil da Empresa*/


Route::get('empresa.edita.perfil/{id}', 'EmpresaController@EmpresaEditaPerfil')->name('empresa.edita.perfil');

Route::post('empresa.salva.perfil.editado/{id}', 'EmpresaController@EmpresaSalvaPerfilEditado')->name('empresa.salva.perfil.editado');

/*Finalizar Ordem de Serviço*/

Route::get('empresa.finaliza.ordem.servico.pf/{id}', 'EmpresaController@EmpresaFinalizaOrdemServicoClientePF')->name('empresa.finaliza.ordem.servico.pf');

Route::post('empresa.salva.ordem.servico.cliente.pf.finalizada/{id}', 'EmpresaController@EmpresaSalvaOrdemServicoClientePFFinalizada')->name('empresa.salva.ordem.servico.cliente.pf.finalizada');



/*Editar Produto*/

Route::get('empresa.edita.produto/{id}', 'EmpresaController@EmpresaEditaProduto')->name('empresa.edita.produto');

Route::post('empresa.salva.produto.editado/{id}', 'EmpresaController@EmpresaSalvaProdutoEditado')->name('empresa.salva.produto.editado');

/*Deletar Produto*/
Route::delete('empresa.deleta.produto/{id}', 'EmpresaController@EmpresaDeletaProduto')->name('empresa.deleta.produto');


/*Edição de Foto*/

Route::get('empresa.edita.foto/{id}', 'EmpresaController@EmpresaEditaFoto')->name('empresa.edita.foto');

Route::post('empresa.salva.foto.editada', 'EmpresaController@EmpresaSalvaFotoEditada')->name('empresa.salva.foto.editada');
/**/

/*

/*Listagem de Compras*/

Route::get('empresa.lista.compra', 'EmpresaController@EmpresaListaCompra')->name('empresa.lista.compra');


Route::get('empresa.cadastro.compra', 'EmpresaController@EmpresaCadastroCompra')->name('empresa.cadastro.compra');

/*


/*Ajax Quantidade de Produtos*/

Route::get('ajax/pegar-quantidade-produtos','EmpresaController@getQuantidadeProdutoAjax');

/*Cadatro de Compras*/

Route::post('empresa.cadastra.compra', 'EmpresaController@EmpresaCadastraCompra')->name('empresa.cadastra.compra');


/*Lista de Orçamentos*/

Route::get('empresa.lista.orcamento.pf', 'EmpresaController@EmpresaListaOrcamentoPF')->name('empresa.lista.orcamento.pf');

/*Formulário de Cadastro Orçamento PF*/

Route::get('empresa.cadastro.orcamento.pf', 'EmpresaController@EmpresaCadastroOrcamentoPF')->name('empresa.cadastro.orcamento.pf');


Route::post('empresa.cadastra.orcamento.pf', 'EmpresaController@EmpresaCadastraOrcamentoPF')->name('empresa.cadastra.orcamento.pf');

/*Lista de Fornecedores*/

Route::get('empresa.lista.fornecedor', 'EmpresaController@EmpresaListaFornecedor')->name('empresa.lista.fornecedor');

/*Cadastro de Fornecedor*/

Route::get('empresa.cadastro.fornecedor', 'EmpresaController@EmpresaCadastroFornecedor')->name('empresa.cadastro.fornecedor');
Route::post('empresa.cadastra.fornecedor', 'EmpresaController@EmpresaCadastraFornecedor')->name('empresa.cadastra.fornecedor');

/*Deletar Cliente PJ*/
Route::delete('empresa.deleta.cliente.pj/{id}', 'EmpresaController@EmpresaDeletaClientePj')->name('empresa.deleta.cliente.pj');


Route::get('empresa.lista.ordem.servico.pj', 'EmpresaController@EmpresaListaOrdemServicoPJ')->name('empresa.lista.ordem.servico.pj');

Route::get('empresa.cadastro.ordem.servico.pj', 'EmpresaController@EmpresaCadastroOrdemServicoPJ')->name('empresa.cadastro.ordem.servico.pj');

Route::post('empresa.cadastra.ordem.servico.pj', 'EmpresaController@EmpresaCadastraOrdemServicoPJ')->name('empresa.cadastra.ordem.servico.pj');


/*Cadastro de Cargo e Salario*/

Route::get('empresa.cadastro.cargo.salario', 'EmpresaController@EmpresaCadastroCargoSalario')->name('empresa.cadastro.cargo.salario');



/*Lista de Cargos e Salários*/


Route::get('empresa.lista.cargo.salario', 'EmpresaController@EmpresaListaCargoSalario')->name('empresa.lista.cargo.salario');


/*Método Para Cadastrar o Cargo e o Salário*/

Route::post('empresa.cadastra.cargo.salario', 'EmpresaController@EmpresaCadastraCargoSalario')->name('empresa.cadastra.cargo.salario');

/*Página da Agenda*/

Route::get('empresa.lista.agenda', 'EmpresaController@EmpresaListaAgenda')->name('empresa.lista.agenda');



});

/*
SQL

INSERT INTO estados VALUES (1, "Rio Grande do Sul", "RS");
INSERT INTO cidades VALUES (1, "Pelotas", 1);








*/