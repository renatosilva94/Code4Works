<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticable;

class Usuarios extends Authenticable
{
    public $timestamps = false;
    
    protected $fillable = array('nome', 'sobrenome', 'email', 'password', 'sexo', 'telefone', 'tipo_usuario', 'ativo');
    
    protected $hidden = array('password', 'remember_token');
    
   
}
