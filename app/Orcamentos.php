<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orcamentos extends Model {

    public $timestamps = false;
    protected $fillable = array('cliente_id', 'nome_orcamento');


    public function cliente() {
        return $this->belongsTo('App\Clientes');
    }


    public function produto() {
        return $this->belongsToMany('App\Produtos', 'orcamentos_produtos')->withPivot('valor_total', 'quantidade', 'valor_unitario');
    }

}
