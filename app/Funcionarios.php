<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcionarios extends Model {

    public $timestamps = false;
    protected $fillable = array('nome', 'sobrenome', 'rg', 'cpf', 'cep', 'bairro', 'rua', 'numero', 'password', 'pispasep', 'email', 'ativo', 'usuario_id', 'cargo_salario_id');
        protected $hidden = array('password', 'remember_token');


    public function estado() {
        return $this->belongsTo('App\Estados');
    }
    
    public function cidade() {
        return $this->belongsTo('App\Cidades');
    }

	public function usuario() {
        return $this->belongsTo('App\Usuarios');
    }

    public function cargo_salario() {
        return $this->belongsTo('App\CargosSalarios');
    }



}
