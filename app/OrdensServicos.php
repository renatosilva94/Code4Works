<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdensServicos extends Model {

    public $timestamps = false;
    protected $fillable = array('usuario_id','cliente_id', 'funcionario_id', 'data', 'hora', 'problema_relatado', 'status', 'valor_total', 'solucao', 'tipo_cliente');

    public function funcionario() {
        return $this->belongsTo('App\Funcionarios');
    }
    
    public function cliente() {
        return $this->belongsTo('App\Clientes');
    }

 



}
