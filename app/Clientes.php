<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model {

    public $timestamps = false;
    protected $fillable = array('usuario_id','nome_cliente', 'telefone', 'cpf', 'cep', 'bairro', 'rua', 'numero', 'email', 'tipo_cliente', 'ativo', 'estado_id', 'cidade_id', 'cnpj', 'razao_social', 'nome_fantasia');

    public function estado() {
        return $this->belongsTo('App\Estados');
    }
    
    public function cidade() {
        return $this->belongsTo('App\Cidades');
    }

}
