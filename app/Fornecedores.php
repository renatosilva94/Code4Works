<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fornecedores extends Model {

    public $timestamps = false;
    protected $fillable = array('nome_fantasia', 'razao_social', 'cnpj', 'cep', 'bairro', 'rua', 'numero', 'telefone', 'email' , 'estado_id', 'cidade_id', 'tipo_cliente', 'ativo');

    public function estado() {
        return $this->belongsTo('App\Estados');
    }
    
    public function cidade() {
        return $this->belongsTo('App\Cidades');
    }

}
