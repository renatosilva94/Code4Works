<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compras extends Model {

    public $timestamps = false;
    protected $fillable = array('numero_nota_fiscal', 'quantidade_embalagens', 'preco_embalagem', 'quantidade_produtos_embalagem', 'preco_unitario', 'preco_total', 'produto_id');

   

}
