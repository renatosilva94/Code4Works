<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produtos extends Model {

    public $timestamps = false;
    protected $fillable = array('nome_produto', 'quantidade_estoque', 'data_compra', 'codigo_barras' );


 public function fornecedor() {
        return $this->belongsToMany('App\Fornecedores');
  }

  public function orcamento(){
  	    return $this->belongsToMany('App\Orcamentos', 'orcamentos_produtos')->withPivot('valor_total', 'quantidade', 'valor_unitario');
  }

}
