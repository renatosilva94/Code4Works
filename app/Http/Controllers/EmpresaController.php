<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Usuarios;
use App\Estados;
use App\Cidades;
use App\Funcionarios;
use App\Produtos;
use App\CargosSalarios;
use App\Compras;
use App\Fornecedores;
use App\Clientes;
use App\OrdensServicos;
use App\Orcamentos;
use Illuminate\Support\Facades\Input;
use Auth;
use \View;



class EmpresaController extends Controller {
    /* Página de Login */

    public function EmpresaLogin() {

        return view('empresa_login');
    }

    /* Método para Efetuar Login */

    public function EmpresaLoginEfetuar(Request $request) {


        $validator = validator($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('empresa.login')->withErrors($validator)->withInput();
        }

        $credentials = ['email' => $request->get('email'), 'password' => $request->get('password')];

        if (auth()->guard('usuarios')->attempt($credentials)) {

            return redirect()->route('empresa.dashboard.principal');
        } else {
            return redirect('empresa.login')->withErrors(['errors' => 'Login Inválido'])->withInput();
        }
    }

    /* Página de Dashboard da Empresa */

    public function EmpresaDashboardPrincipal() {


        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;


        return view('empresa_dashboard_principal', compact('usuario_autenticado_id', 'usuario_autenticado_nome'));
    }

    /* Página de Cadastro de Cliente PF */

    public function EmpresaCadastroClientePF() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $acao = 1;

        $cidades = Cidades::orderBy('nome_cidade')->get();
        $estados = Estados::orderBy('nome_estado')->get();

        return view('empresa_cadastro_cliente_pf', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'estados', 'acao'));
    }

    /* Página de Cadastro de Cliente PJ */

    public function EmpresaCadastroClientePJ() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

$acao = 1;

        $cidades = Cidades::orderBy('nome_cidade')->get();
        $estados = Estados::orderBy('nome_estado')->get();

        return view('empresa_cadastro_cliente_pj', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'estados','acao'));
    }

    /* Página de Listagem de Cliente Pessoa Fisica */

    public function EmpresaListaClientePF() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $cidades = Cidades::orderBy('nome_cidade')->get();
        //$clientes_pf = Clientes::paginate(10);
        $clientes_pf = Clientes::where('tipo_cliente', '=', 1)->where('usuario_id', '=', $usuario_autenticado_id )->paginate(10);

        return view('empresa_lista_cliente_pf', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'clientes_pf'));
    }

    /* Página de Listagem de Cliente Pessoa Juridica */

    public function EmpresaListaClientePJ() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $cidades = Cidades::orderBy('nome_cidade')->get();
        $clientes_pj = Clientes::where('tipo_cliente', '=', 2)->where('usuario_id', '=', $usuario_autenticado_id)->paginate(10);

        return view('empresa_lista_cliente_pj', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'clientes_pj'));
    }

    /* Página de Cadastro */

    public function EmpresaCadastro() {


        return view('empresa_cadastro');
    }

    /* Método para Efetuar o Cadastro */

    public function EmpresaCadastroEfetuar(Request $request) {


        $tipo_usuario = 1;
        $ativo = 0;

        $usuarios = Usuarios::create([
                    'nome' => $request['nome'],
                    'sobrenome' => $request['sobrenome'],
                    'email' => $request['email'],
                    'password' => bcrypt($request['password']),
                    'sexo' => $request['sexo'],
                    'telefone' => $request['telefone'],
                    'tipo_usuario' => $tipo_usuario,
                    'ativo' => $ativo
        ]);


        if ($usuarios) {
            return redirect()->route('empresa.login', compact('usuario_autenticado_id', 'usuario_autenticado_nome'));
        }
    }

    /* Efetuar Consulta A Cidades Baseadas No Estado - AJAX */

    public function ListaCidadesEstados(Request $request) {
        $cidadesAjax = DB::table("cidades")
                ->where("estado_id", $request->estado_id)
                ->pluck("nome_cidade", "id");
        return response()->json($cidadesAjax);
    }

    public function EmpresaCadastraClientePF(Request $request) {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;
        
        /* Tipo de Cliente 1 = Pessoa Física / Ativo 1 = ATIVO */

        $tipo_cliente = 1;
        $ativo = 1;

        $clientesPf = Clientes::create([
                    'nome_cliente' => $request['nome_cliente'],
                    'telefone' => $request['telefone'],
                    'cpf' => $request['cpf'],
                    'cep' => $request['cep'],
                    'bairro' => $request['bairro'],
                    'estado_id' => $request['estado_id'],
                    'cidade_id' => $request['cidade_id'],
                    'rua' => $request['rua'],
                    'numero' => $request['numero'],
                    'email' => $request['email'],
                    'tipo_cliente' => $tipo_cliente,
                    'ativo' => $ativo,
                    'usuario_id' => $usuario_autenticado_id
        ]);

        if ($clientesPf) {
            return redirect()->route('empresa.lista.cliente.pf', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'acao'));
        }
    }

    public function EmpresaCadastraClientePJ(Request $request) {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        /* Tipo de Cliente 2 = Pessoa Juridica / Ativo 1 = ATIVO */

        $tipo_cliente = 2;
        $ativo = 1;

        $clientesPj = Clientes::create([
                    'nome_fantasia' => $request['nome_fantasia'],
                    'razao_social' => $request['razao_social'],
                    'cnpj' => $request['cnpj'],
                    'cep' => $request['cep'],
                    'bairro' => $request['bairro'],
                    'estado_id' => $request['estado_id'],
                    'cidade_id' => $request['cidade_id'],
                    'rua' => $request['rua'],
                    'numero' => $request['numero'],
                    'telefone' => $request['telefone'],
                    'email' => $request['email'],
                    'tipo_cliente' => $tipo_cliente,
                    'ativo' => $ativo,
                    'usuario_id' => $usuario_autenticado_id

        ]);

        if ($clientesPj) {
            return redirect()->route('empresa.lista.cliente.pj', compact('usuario_autenticado_id', 'usuario_autenticado_nome'));
        }
    }

    public function EmpresaListaClientePFFiltros(Request $request) {


        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $cidade_id = $request->cidade_id;

        $cidades = Cidades::orderBy('nome_cidade')->get();

        $filtro = array();



        if (!empty($cidade_id)) {
            array_push($filtro, array('cidade_id', 'like', '%' . $cidade_id . '%'));
        }

        $clientes_pf = Clientes::where($filtro)
                ->orderBy('cidade_id')
                ->paginate(5);



        return View('empresa_lista_cliente_pf', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'clientes_pf', 'tipo_cliente', 'cidade_id'));
    }

    public function EmpresaListaClientePJFiltros(Request $request) {


        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $cidade_id = $request->cidade_id;

        $cidades = Cidades::orderBy('nome_cidade')->get();

        $filtro = array();



        if (!empty($cidade_id)) {
            array_push($filtro, array('cidade_id', 'like', '%' . $cidade_id . '%'));
        }

        $clientes_pj = Clientes::where($filtro)
                ->orderBy('cidade_id')
                ->paginate(5);



        return View('empresa_lista_cliente_pj', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'clientes_pj', 'tipo_cliente', 'cidade_id'));
    }

    public function EmpresaCadastroFuncionario() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $acao = 1;

        $cargos_salarios = CargosSalarios::orderBy('nome_cargo')->get();
        $cidades = Cidades::orderBy('nome_cidade')->get();
        $estados = Estados::orderBy('nome_estado')->get();

        return view('empresa_cadastro_funcionario', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'estados', 'acao', 'cargos_salarios'));
    }

    public function EmpresaCadastraFuncionario(Request $request) {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        /* Tipo de Cliente 1 = Pessoa Física / Ativo 1 = ATIVO */


        $ativo = 1;

        $funcionarios = Funcionarios::create([
                    'nome' => $request['nome'],
                    'sobrenome' => $request['sobrenome'],
                    'rg' => $request['rg'],
                    'cpf' => $request['cpf'],
                    'cep' => $request['cep'],
                    'estado_id' => $request['estado_id'],
                    'cidade_id' => $request['cidade_id'],
                    'bairro' => $request['bairro'],
                    'rua' => $request['rua'],
                    'numero' => $request['numero'],
                    'password' => bcrypt($request['password']),
                    'pispasep' => $request['pispasep'],
                    'cargo_salario_id' => $request['cargo_salario_id'],
                    'email' => $request['email'],
                    'ativo' => $ativo,
                    'usuario_id' => $usuario_autenticado_id
        ]);

        if ($funcionarios) {
            return redirect()->route('empresa.lista.funcionario', compact('usuario_autenticado_id', 'usuario_autenticado_nome'));
        }
    }
    
    
    public function EmpresaEditaClientePf($id) {
        
        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;
        
        $cidades = Cidades::orderBy('nome_cidade')->get();
        $estados = Estados::orderBy('nome_estado')->get();

        $clientePfId = Clientes::find($id);

        $acao = 2;

        return view('empresa_cadastro_cliente_pf', compact('usuario_autenticado_id','usuario_autenticado_nome','acao', 'clientePfId', 'cidades', 'estados'));
    }
    
    
    public function EmpresaSalvaClientePfEditado(Request $request, $id) {

        $this->validate($request, [
            'nome_cliente' => 'required',
            'telefone' => 'required',
            'cpf' => 'required',
            'cep' => 'required',
            'bairro' => 'required',
            'rua' => 'required',
            'numero' => 'required',
            'email' => 'required',
            'estado_id' => 'required',
            'cidade_id' => 'required',
            
        ]);

        $dados = $request->all();

        $clientePfId = Clientes::find($id);

        $alt = $clientePfId->update($dados);

        if ($alt) {
            return redirect()->route('empresa.lista.cliente.pf');
        }
    }
    
    public function EmpresaDeletaClientePf($id) {
        $clientePf = Clientes::find($id);
        if ($clientePf->delete()) {
            return redirect()->route('empresa.lista.cliente.pf');
        }
    }


    public function EmpresaListaFuncionario() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $cidades = Cidades::orderBy('nome_cidade')->get();
        $funcionarios = Funcionarios::paginate(10);

        return view('empresa_lista_funcionario', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'funcionarios'));
    }


     public function EmpresaEditaClientePj($id) {
        
        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;
        
        $cidades = Cidades::orderBy('nome_cidade')->get();
        $estados = Estados::orderBy('nome_estado')->get();

        $clientePjId = Clientes::find($id);

        $acao = 2;

        return view('empresa_cadastro_cliente_pj', compact('usuario_autenticado_id','usuario_autenticado_nome','acao', 'clientePjId', 'cidades', 'estados'));
    }

    public function EmpresaSalvaClientePjEditado(Request $request, $id) {

        $this->validate($request, [
            'nome_fantasia' => 'required',
            'razao_social' => 'required',
            'cnpj' => 'required',
            'cep' => 'required',
            'bairro' => 'required',
            'rua' => 'required',
            'numero' => 'required',
            'telefone' => 'required',
            'email' => 'required',
            'estado_id' => 'required',
            'cidade_id' => 'required',
            
        ]);

        $dados = $request->all();

        $clientePjId = Clientes::find($id);

        $alt = $clientePjId->update($dados);

        if ($alt) {
            return redirect()->route('empresa.lista.cliente.pj');
        }
    }


public function EmpresaListaProduto() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $produtos = Produtos::paginate(10);
                $cidades = Cidades::orderBy('nome_cidade')->get();


        return view('empresa_lista_produto', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'produtos', 'cidades'));
    }


public function EmpresaCadastroProduto() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $acao = 1;


        return view('empresa_form_produto', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'acao'));
    }
    

    public function EmpresaCadastraProduto(Request $request) {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $produtos = Produtos::create([
                    'nome_produto' => $request['nome_produto'],
                    'quantidade_estoque' => $request['quantidade_estoque'],
                    'data_compra' => $request['data_compra'],
                    'codigo_barras' => $request['codigo_barras']
                    
        ]);

        if ($produtos) {
            return redirect()->route('empresa.dashboard.principal', compact('usuario_autenticado_id', 'usuario_autenticado_nome'));
        }
    }

public function EmpresaEditaFuncionario($id) {
        
        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;
        
        $cidades = Cidades::orderBy('nome_cidade')->get();
        $estados = Estados::orderBy('nome_estado')->get();

        $funcionarioId = Funcionarios::find($id);

        $acao = 2;

        return view('empresa_cadastro_funcionario', compact('usuario_autenticado_id','usuario_autenticado_nome','acao', 'funcionarioId', 'cidades', 'estados'));
    }



    public function EmpresaSalvaFuncionarioEditado(Request $request, $id) {

        $this->validate($request, [
            'nome' => 'required',
            'sobrenome' => 'required',
            'rg' => 'required',
            'cpf' => 'required',
            'cep' => 'required',
            'bairro' => 'required',
            'rua' => 'required',
            'numero' => 'required',
            'pispasep' => 'required',
            'cargo' => 'required',
            'email' => 'required'
            
        ]);

        $dados = $request->all();

        $funcionarioId = Funcionarios::find($id);

        $alt = $funcionarioId->update($dados);

        if ($alt) {
            return redirect()->route('empresa.lista.funcionario');
        }
    }

       public function EmpresaEfetuarLogout() {

        auth()->guard('usuarios')->logout();

        return redirect('empresa.login');
    }


 public function EmpresaListaOrdemServicoPF() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $cidades = Cidades::orderBy('nome_cidade')->get();

        

        //$ordens_servico_pf = OrdensServicos::where('')->paginate(10);

        $ordens_servico_pf = OrdensServicos::where('tipo_cliente', '=', 1)->where('usuario_id', '=', $usuario_autenticado_id)->paginate(10);


        return view('empresa_lista_ordem_servico_pf', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'ordens_servico_pf'));
    }

public function EmpresaCadastroOrdemServicoPF() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $cidades = Cidades::orderBy('nome_cidade')->get();
        $estados = Estados::orderBy('nome_estado')->get();



        $clientes_pfs = Clientes::where('tipo_cliente','=',1)->where('usuario_id', '=', $usuario_autenticado_id)->orderBy('nome_cliente')->get();


        $funcionarios = Funcionarios::orderBy('nome')->get();
        

        return view('empresa_form_ordem_servico_pf', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'estados', 'clientes_pfs', 'funcionarios'));
    }


public function EmpresaCadastraOrdemServicoPF(Request $request) {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;
        
        $status = 0;
        $null = "";

        //$tipo_cliente = 1;
        /* Tipo de Cliente 1 = Pessoa Física / Ativo 1 = ATIVO */

        $ordens_servico_pf = OrdensServicos::create([
                    'data' => $request['data'],
                    'hora' => $request['hora'],
                    'funcionario_id' => $request['funcionario_id'],
                    'cliente_id' => $request['cliente_id'],
                    'problema_relatado' => $request['problema_relatado'],
                    'status' => $status,
                    'valor_total' => 0,
                    'solucao' => $null,
                    'tipo_cliente' => 1,
                    'usuario_id' => $usuario_autenticado_id


                             
        ]);

        if ($ordens_servico_pf) {
            return redirect()->route('empresa.lista.ordem.servico.pf', compact('usuario_autenticado_id', 'usuario_autenticado_nome'));
        }
    }


public function EmpresaGerarPdfOrdemServicoPF($id){

        $usuario_autenticado_sobrenome = Auth::guard('usuarios')->user()->sobrenome;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $ordem_servico_pf = OrdensServicos::find($id);

 
    $pdf  =  \App::make('dompdf.wrapper');
    $view =  View::make('empresa_pdf_ordem_servico_pf', compact('ordem_servico_pf', 'usuario_autenticado_nome', 'usuario_autenticado_sobrenome'))->render();
    $pdf->loadHTML($view);
    return $pdf->stream();
}


 public function EmpresaDeletaOrdemServicoPF($id) {
        $ordem_servico_pf = OrdensServicos::find($id);
        if ($ordem_servico_pf->delete()) {
            return redirect()->route('empresa.lista.ordem.servico.pf');
        }
    }



public function EmpresaEditaPerfil($id){

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $usuarioId = Usuarios::find($id);

return view('empresa_edita_perfil', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'usuarioId'));



}

 public function EmpresaSalvaPerfilEditado(Request $request, $id) {

        $this->validate($request, [
            'nome' => 'required',
            'sobrenome' => 'required',
            'email' => 'required',
            'sexo' => 'required',
            'telefone' => 'required',

            
        ]);

        $dados = $request->all();

        $usuarioId = Usuarios::find($id);

        $alt = $usuarioId->update($dados);

        if ($alt) {
            return redirect()->route('empresa.dashboard.principal');
        }
    }




 public function EmpresaFinalizaOrdemServicoClientePF($id) {
        
        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;
        

        $ordemServicoPfId = OrdensServicos::find($id);


        return view('empresa_finaliza_ordem_servico_pf', compact('usuario_autenticado_id','usuario_autenticado_nome', 'ordemServicoPfId'));
    }




public function EmpresaSalvaOrdemServicoClientePFFinalizada(Request $request, $id) {


        $this->validate($request, [
            'valor_total' => 'required',
            'solucao' => 'required'   
        ]);



    $dados = $request->all();

    $ordemServicoPfId = OrdensServicos::find($id);

    DB::table('ordens_servicos')->where('id', $id)->update(['status' => 1]);    

        $alt = $ordemServicoPfId->update($dados);


        if ($alt) {
            return redirect()->route('empresa.lista.ordem.servico.pf');
        }
    }



      public function EmpresaEditaProduto($id) {
        
        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;
        
        $cidades = Cidades::orderBy('nome_cidade')->get();
        $estados = Estados::orderBy('nome_estado')->get();

        $produtoId = Produtos::find($id);

        $acao = 2;

        return view('empresa_form_produto', compact('usuario_autenticado_id','usuario_autenticado_nome','acao', 'produtoId', 'cidades', 'estados'));
    }
    
    
    public function EmpresaSalvaProdutoEditado(Request $request, $id) {

        $this->validate($request, [
            'nome_produto' => 'required',
            'quantidade_estoque' => 'required',
            'codigo_barras' => 'required',
            'data_compra' => 'required',
            
        ]);

        $dados = $request->all();

        $produtoId = Produtos::find($id);

        $alt = $produtoId->update($dados);

        if ($alt) {
            return redirect()->route('empresa.lista.produto');
        }
    }

    

     public function EmpresaDeletaProduto($id) {
        $produtoId = Produtos::find($id);
        if ($produtoId->delete()) {
            return redirect()->route('empresa.lista.produto');
        }
    }


     public function EmpresaEditaFoto($id) {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $empresaId = Usuarios::find($id);

        return view('empresa_edita_foto', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'empresaId'));
    }

    public function EmpresaSalvaFotoEditada(Request $request){



         $dados = $request->all();

        if (isset($dados['foto'])) {

            $id = $dados['id'];
            $fotoId = $id . '.jpg';
            $request->foto->move(public_path('fotos'), $fotoId);
        }

        return redirect()->route('empresa.dashboard.principal');
    }

    public function EmpresaListaCompra(){

       $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $cidades = Cidades::orderBy('nome_cidade')->get();
        $compras = Compras::paginate(10);

        return view('empresa_lista_compra', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'compras'));
    }

    public function EmpresaCadastroCompra() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $acao = 1;

        $cidades = Cidades::orderBy('nome_cidade')->get();
        $estados = Estados::orderBy('nome_estado')->get();
        $produtos = Produtos::orderBy('nome_produto')->get();


        return view('empresa_form_compra', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'estados', 'acao', 'produtos'));
    }




    public function getQuantidadeProdutoAjax(Request $request)
    {
        $quantidadeProdutosAjax = DB::table("produtos")
                    ->where("id",$request->id)
                    ->pluck("quantidade_estoque","id");
        return response()->json($quantidadeProdutosAjax);
    }


    public function EmpresaCadastraCompra(Request $request) {
        $compras = Compras::create([
                    'numero_nota_fiscal' => $request['numero_nota_fiscal'],
                    'quantidade_embalagens' => $request['quantidade_embalagens'],
                    'preco_embalagem' => $request['preco_embalagem'],
                    'quantidade_produtos_embalagem' => $request['quantidade_produtos_embalagem'],
                    'preco_unitario' => $request['preco_unitario'],
                    'preco_total' => $request['preco_total'],
                    'produto_id' => $request['produto_id']                    

        ]);

        if ($compras) {

             $abasteceEstoque = DB::table('produtos')
            ->where('id', $request['produto_id'])
            ->update(['quantidade_estoque' => (DB::raw($request['quantidadeEstoquePosterior']))]);


            return redirect()->route('empresa.lista.compra');
        }
    }



 public function EmpresaListaOrcamentoPF() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $cidades = Cidades::orderBy('nome_cidade')->get();
        $orcamentos = Orcamentos::paginate(10);


        return view('empresa_lista_orcamento_pf', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'orcamentos'));
    }


    public function EmpresaCadastroOrcamentoPF() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $acao = 1;

        $produtos = Produtos::orderBy('nome_produto')->get();
        $clientes_pfs = Clientes::orderBy('nome_cliente')->where('tipo_cliente', 1)->get();

        return view('empresa_form_orcamento_pf', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'produtos', 'clientes_pfs', 'acao'));
    }



    public function EmpresaCadastraOrcamentoPF(Request $request) {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;
        
        /* Tipo de Cliente 1 = Pessoa Física / Ativo 1 = ATIVO */

       // for ($i = 0; $i < count($request->produto_id); $i++) {
        $orcamento_pf = Orcamentos::create([
            //'name' => $request->name[$i],
                    'cliente_id' => $request->cliente_id,
                    'nome_orcamento' => $request->nome_orcamento,
                 // 'valor_total' => $request->valor_total[$i],
                 //  'quantidade' => $request->quantidade[$i],
                 //  'valor_unitario' => $request->valor_unitario[$i]
       ]);


           

//}


        if ($orcamento_pf) {
         
        // $orcamento_pf->produto()->sync(array(1,2));
        $orcamento_pf->produto()->sync([1,2]);
            

            return redirect()->route('empresa.dashboard.principal', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'acao'));
        }
    }



     public function EmpresaListaFornecedor() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $cidades = Cidades::orderBy('nome_cidade')->get();
        $fornecedores = Fornecedores::paginate(10);

        return view('empresa_lista_fornecedor', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'fornecedores'));
    }

public function EmpresaCadastroFornecedor() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $acao = 1;

        $cidades = Cidades::orderBy('nome_cidade')->get();
        $estados = Estados::orderBy('nome_estado')->get();

        return view('empresa_form_fornecedor', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'estados', 'acao'));
    }




    public function EmpresaCadastraFornecedor(Request $request) {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        /* Tipo de Cliente 1 = Pessoa Física / Ativo 1 = ATIVO */

        $tipo_cliente = 2;
        $ativo = 1;

        $fornecedores = Fornecedores::create([
                    'nome_fantasia' => $request['nome_fantasia'],
                    'razao_social' => $request['razao_social'],
                    'cnpj' => $request['cnpj'],
                    'cep' => $request['cep'],
                    'bairro' => $request['bairro'],
                    'estado_id' => $request['estado_id'],
                    'cidade_id' => $request['cidade_id'],
                    'rua' => $request['rua'],
                    'numero' => $request['numero'],
                    'telefone' => $request['telefone'],
                    'email' => $request['email'],
                    'tipo_cliente' => $tipo_cliente,
                    'ativo' => $ativo
        ]);

        if ($fornecedores) {
            return redirect()->route('empresa.lista.fornecedor', compact('usuario_autenticado_id', 'usuario_autenticado_nome'));
        }
    }

    public function EmpresaDeletaClientePj($id) {
        $clientePj = Clientes::find($id);
        if ($clientePj->delete()) {
            return redirect()->route('empresa.lista.cliente.pj');
        }
    }





    public function EmpresaListaOrdemServicoPJ() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $cidades = Cidades::orderBy('nome_cidade')->get();

        

        //$ordens_servico_pf = OrdensServicos::where('')->paginate(10);

        $ordens_servico_pj = OrdensServicos::where('tipo_cliente', '=', 2)->where('usuario_id', '=', $usuario_autenticado_id)->paginate(10);


        return view('empresa_lista_ordem_servico_pj', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'ordens_servico_pj'));
    }


public function EmpresaCadastroOrdemServicoPJ() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $cidades = Cidades::orderBy('nome_cidade')->get();
        $estados = Estados::orderBy('nome_estado')->get();



        $clientes_pjs = Clientes::where('tipo_cliente','=',2)->where('usuario_id', '=', $usuario_autenticado_id)->orderBy('razao_social')->get();


        $funcionarios = Funcionarios::orderBy('nome')->get();
        

        return view('empresa_form_ordem_servico_pj', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'estados', 'clientes_pjs', 'funcionarios'));
    }


public function EmpresaCadastraOrdemServicoPJ(Request $request) {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;
        
        $status = 0;
        $null = "";

        //$tipo_cliente = 1;
        /* Tipo de Cliente 1 = Pessoa Física / Ativo 1 = ATIVO */

        $ordens_servico_pj = OrdensServicos::create([
                    'data' => $request['data'],
                    'hora' => $request['hora'],
                    'funcionario_id' => $request['funcionario_id'],
                    'cliente_id' => $request['cliente_id'],
                    'problema_relatado' => $request['problema_relatado'],
                    'status' => $status,
                    'valor_total' => 0,
                    'solucao' => $null,
                    'tipo_cliente' => 2,
                    'usuario_id' => $usuario_autenticado_id

                             
        ]);

        if ($ordens_servico_pj) {
            return redirect()->route('empresa.lista.ordem.servico.pj', compact('usuario_autenticado_id', 'usuario_autenticado_nome'));
        }
    }


        public function EmpresaCadastroCargoSalario() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $acao = 1;


        return view('empresa_form_cargo_salario', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'acao'));
    }





    public function EmpresaListaCargoSalario() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $cidades = Cidades::orderBy('nome_cidade')->get();
        //$clientes_pf = Clientes::paginate(10);
        $cargossalarios = CargosSalarios::paginate(10);

        return view('empresa_lista_cargo_salario', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'cargossalarios'));
    }




public function EmpresaCadastraCargoSalario(Request $request) {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;
        

        $cargo_salario = CargosSalarios::create([
                    'nome_cargo' => $request['nome_cargo'],
                    'salario' => $request['salario']
                    

                             
        ]);

        if ($cargo_salario) {
            return redirect()->route('empresa.lista.cargo.salario', compact('usuario_autenticado_id', 'usuario_autenticado_nome'));
        }
    }






    public function EmpresaListaAgenda() {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $cidades = Cidades::orderBy('nome_cidade')->get();
        $fornecedores = Fornecedores::paginate(10);

        return view('empresa_lista_agenda', compact('usuario_autenticado_id', 'usuario_autenticado_nome', 'cidades', 'fornecedores'));
    }


    }


