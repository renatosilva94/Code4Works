@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Compras
            <small>Lista de Compras</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Compras</li>
            <li class="active">Lista de Compras</li>
        </ol>   
    </section>

    <form method="post" action="#">
        {{ csrf_field() }}

        <div class="selectSearch">


            <div class="col-sm-4">
                <div class="form-group">
                    <label for="cidade_id">Cidade:</label>
                    <select class="form-control" id="cidade_id" name="cidade_id">
                        <option value=""></option>

                        @foreach($cidades as $cidade)    
                        <option value="{{$cidade->id}}">{{$cidade->nome_cidade}}</option>
                        @endforeach    
                    </select>
                </div>
            </div>


            <div class='col-sm-6 btnPesquisar'>
                <button type="submit" class="btn btn-bitbucket">Pesquisar</button> 
                <a href="{{route('empresa.cadastro.compra')}}" class="btn btn-outline btn-success">Cadastro de Compra</a>            
            </div>
        </div>
    </form>

    <div class='col-sm-12'>

        @if (count($compras)==0)
        <div class="alert alert-danger">
            Não Existem Compras Com O Filtro Informado
        </div>
        @endif
    </div>

    <div class="col-sm-12 boxCenter boxCentralize">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Lista de Compras</h3>
                </div>
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="thListagem">Código</th>
                                <th class="thListagem">Número da Nota Fiscal</th>
                                <th class="thListagem">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($compras as $compra)

                            <tr>

                                
                                <td class="tdListagem">{{$compra->id}}</td>
                                <td class="tdListagem">{{$compra->numero_nota_fiscal}}</td>


                                
                                <td>
                                    

                                    

                                    <form style="display: inline-block"
                          method="post"
                          action="#"
                          onsubmit="return confirm('Confirma Exclusão da Compra?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger btn-sm"> Excluir </button>
                    </form> 

                                    

                                     <a href="#" 
                                       class="btn btn-warning btn-sm" 
                                       role="button">Detalhes
                                    </a>

                               


















                                </td>

                                @endforeach

                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>  
        </div>
    </div>
</div>
@endsection