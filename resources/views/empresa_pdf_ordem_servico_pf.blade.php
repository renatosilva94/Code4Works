<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<body>

<div class='col-sm-11'>
    <h2 align="center"> Ordem de Serviço Nº {{$ordem_servico_pf->id}} - {{$usuario_autenticado_nome}} {{$usuario_autenticado_sobrenome}} 
</h2>
</div>

<div class='col-sm-12'>


<table>
    <thead>
      <tr>
        <th></th>

        <th>Dados do Cliente</th>
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><b>Nome do Cliente:</b> </td>
        <td>{{$ordem_servico_pf->cliente->nome_cliente}}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><b>CPF:</b> </td>
        <td>{{$ordem_servico_pf->cliente->cpf}}</td>
      </tr>
      
      <tr>
        <td><b>CEP:</b> </td>
        <td>{{$ordem_servico_pf->cliente->cep}}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><b>Bairro:</b> </td>
        <td>{{$ordem_servico_pf->cliente->bairro}}</td>
      </tr>
      
       <tr>
          <td><b>Rua:</b> </td>
        <td>{{$ordem_servico_pf->cliente->rua}}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
<td><b>Número:</b> </td>
        <td>{{$ordem_servico_pf->cliente->numero}}</td>
      </tr>
        
      <tr>
        <td><b>E-Mail:</b> </td>
        <td>{{$ordem_servico_pf->cliente->email}}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><b>Funcionário Responsável:</b> </td>
        <td>{{$ordem_servico_pf->funcionario->nome}}</td>

      </tr>

      <tr> 
      <td><b>Problema Relatado:</b> </td>
      <td>{{$ordem_servico_pf->problema_relatado}}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
<td><b>Tipo de Equipamento:</b> </td>
      <td>{{$ordem_servico_pf->tipo_equipamento}}</td>
        

      </tr>
      
      
    </tbody>
  </table>





<br>
<br>






<table>
    <thead>
      <tr>
        <th>Informações: </th>
        
      </tr>
    </thead>
    <tbody>
      <tr>

       <td>________________________________________________________________________________________</td>


      </tr>
      <tr>

       <td>________________________________________________________________________________________</td>


      </tr>
      
      <tr>
                 <td>________________________________________________________________________________________</td>

      </tr>

      <tr>
                 <td>________________________________________________________________________________________</td>

      </tr>

      <tr>
                 <td>________________________________________________________________________________________</td>

      </tr>
      
    </tbody>
  </table>

<br>
<br>


  <table>
    <thead>
      <tr>
        <th>Encerramento da Ordem de Serviço: </th>
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Assinatura do Técnico: </td>
       <td>_____________________________</td>


      </tr>

      <tr>
           <td>Assinatura do Responsável: </td>
       <td>_____________________________</td>
      </tr>
      
      <tr>
      <td><b>1º Via - Empresa</b></td>
      </tr>
      
    </tbody>
  </table>

<p>------------------------------------------------------------------------------------------------------------------------------------</p>

    
    <!-- Segunda Via -->


    <div class='col-sm-11'>
    <h2 align="center"> Ordem de Serviço Nº {{$ordem_servico_pf->id}} - {{$usuario_autenticado_nome}} {{$usuario_autenticado_sobrenome}}</h2>
    </div>

<table>
    <thead>
      <tr>
        <th>Informações: </th>
        
      </tr>
    </thead>
    <tbody>
      <tr>

       <td>________________________________________________________________________________________</td>


      </tr>
      <tr>

       <td>________________________________________________________________________________________</td>


      </tr>
      
      <tr>
                 <td>________________________________________________________________________________________</td>

      </tr>

      <tr>
                 <td>________________________________________________________________________________________</td>

      </tr>

      <tr>
                 <td>________________________________________________________________________________________</td>

      </tr>
      
    </tbody>
  </table>
<table>
    <thead>
      <tr>
        <th>Encerramento da Ordem de Serviço: </th>
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Assinatura do Técnico: </td>
       <td>_____________________________</td>


      </tr>

      <tr>
           <td>Assinatura do Responsável: </td>
       <td>_____________________________</td>
      </tr>
      
      <tr>
      <td><b>2º Via - Cliente</b></td>
      </tr>
      
    </tbody>
  </table>

</div>
</body>



