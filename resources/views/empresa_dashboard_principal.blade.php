@extends('empresa_dashboard')

@section('conteudo')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            DashBoard
            <small>Painel de Controle</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>    
    </section>

    <section class="content">
        <div class="container bootstrap snippet boxPartidas">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-lg-3 col-sm-3">
                        <div class="circle-tile ">
                            <a href="#"><div class="circle-tile-heading dark-blue"><i class="fa fa-calendar fa-fw fa-3x"></i></div></a>
                            <div class="circle-tile-content dark-blue">

                                <div class="circle-tile-description text-faded">Chamados em Aberto</div>
                                <div class="circle-tile-number text-faded ">0</div>
                                <a class="circle-tile-footer" href="#">Verificar <i class="fa fa-chevron-circle-right"></i></a>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-3 boxCentral">
                        <div class="circle-tile ">
                            <a href="#"><div class="circle-tile-heading green"><i class="fa fa-users fa-fw fa-3x"></i></div></a>
                            <div class="circle-tile-content green">
                                <div class="circle-tile-description text-faded"> Chamados Encerrados </div>
                                <div class="circle-tile-number text-faded ">0</div>
                                <a class="circle-tile-footer" href="#">Verificar <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div> 

                    <div class="col-lg-3 col-sm-3 boxCentral">
                        <div class="circle-tile ">
                            <a href="#"><div class="circle-tile-heading orange"><i class="fa fa-calendar-check-o fa-fw fa-3x"></i></div></a>
                            <div class="circle-tile-content orange">
                                <div class="circle-tile-description text-faded">Chamados Cancelados</div>
                                <div class="circle-tile-number text-faded ">0</div>
                                <a class="circle-tile-footer" href="#">Verificar <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div> 

                    <div class="col-lg-3 col-sm-3 boxCentral">
                        <div class="circle-tile ">
                            <a href="#"><div class="circle-tile-heading red"><i class="fa fa-exclamation-triangle fa-fw fa-3x"></i></div></a>
                            <div class="circle-tile-content red">
                                <div class="circle-tile-description text-faded">Chamados Agendados</div>
                                <div class="circle-tile-number text-faded ">0</div>
                                <a class="circle-tile-footer" href="#">Ver Mais <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div> 
                </div>
            </div> 
        </div>  

</div>

</section>
</div>
@endsection