@extends('empresa_dashboard')

@section('conteudo')


@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Relacionamentos
            <small>Agenda</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Relacionamentos</li>
            <li class="active">Mural</li>
        </ol>   
    </section>

    
    <div class="col-sm-12 boxCenter boxCentralize">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Mural</h3>
                </div>
                <div class="box-body">
                    

                <ul class="timeline">

<!-- timeline time label -->
<li class="time-label">
    <span class="bg-red">
        10 Feb. 2014
    </span>
</li>
<!-- /.timeline-label -->

<!-- timeline item -->
<li>

<!--fa fa-envelope bg-blue = ENVELOPE COM FUNDO AZUL
    fa fa-user bg-aqua = USUARIO COM FUNDO AZUL
    fa fa-comments bg-yellow = ICONE DE COMENTARIO COM FUNDO AMARELO


 -->
    <!-- timeline icon -->
    <i class="fa fa-envelope bg-blue"></i>
    <div class="timeline-item">
        <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

        <h3 class="timeline-header"><a href="#">Support Team</a> ...</h3>

        <div class="timeline-body">
            ...
            Content goes here
        </div>

        <div class="timeline-footer">
            <a class="btn btn-primary btn-xs">...</a>
        </div>
    </div>
</li>
<!-- END timeline item -->

...

</ul>

<div class='col-sm-6 btnPesquisar'>
                <a href="#" class="btn btn-outline btn-success">Novo Evento</a> 
                           
            </div>

                </div>
            </div>  
        </div>
    </div>
</div>
@endsection