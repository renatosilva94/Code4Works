@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Orçamentos
            <small>Lista de Orçamentos</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Orçamentos</li>
            <li class="active">Lista de Orçamentos</li>
        </ol>   
    </section>

    <form method="post" action="#">
        {{ csrf_field() }}

        <div class="selectSearch">
          

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="cidade_id">Cidade:</label>
                    <select class="form-control" id="cidade_id" name="cidade_id">
                        <option value=""></option>

                        @foreach($cidades as $cidade)    
                        <option value="{{$cidade->id}}">{{$cidade->nome_cidade}}</option>
                        @endforeach    
                    </select>
                </div>
            </div>


            <div class='col-sm-1 btnPesquisar'>
                <button type="submit" class="btn btn-bitbucket">Pesquisar</button>            
            </div>
            <a href="{{route('empresa.cadastro.orcamento.pf')}}" class="btn btn-danger">Cadastrar Orçamento PF</a>
        </div>
    </form>

    <div class='col-sm-12'>

         @if (count($orcamentos)==0)
        <div class="alert alert-danger">
            Não Existem Orçamentos Com O Filtro Informado
        </div>
         @endif
    </div>

    <div class="col-sm-12 boxCenter boxCentralize">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Lista de Orçamentos</h3>
                </div>
                <div class="box-body">

                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="thListagem">Código do Orçamento</th>
                                <th class="thListagem">Nome do Cliente</th>
                                <th class="thListagem">Quantidade de Produtos</th>

                                <th class="thListagem">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orcamentos as $orcamento)

                            

                            <tr>

                                <td class="tdListagem">{{$orcamento->id}}</td>
                                <td class="tdListagem">{{$orcamento->cliente->nome_cliente}}</td>
                                <td class="tdListagem"></td>
                                
                                


                                <td>
                                   

                                    <a href="#" 
                                       class="btn btn-success" 
                                       role="button">Editar
                                    </a>

                                    <form style="display: inline-block"
                          method="post"
                          action="#"
                          onsubmit="return confirm('Confirma Exclusão do Orçamento ?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-warning"> Excluir </button>
                    </form> 

  <a href="#" 
                                       class="btn btn-danger" 
                                       role="button">PDF
                                    </a>


                                </td>
                                
                                @endforeach
                                
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>  
        </div>
    </div>
</div>
@endsection