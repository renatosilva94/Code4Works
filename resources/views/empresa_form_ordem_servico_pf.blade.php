@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Ordens de Serviço
            <small>Cadastro de Ordens de Serviço</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Ordens de Serviço</li>
            <li class="active">Cadastro de Ordens de Serviço de Pessoa Física</li>
        </ol>   
    </section>    

    <div class="col-sm-12 boxCenterPartida">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif   

        <form action="{{route('empresa.cadastra.ordem.servico.pf')}}" method="post">



            {{ csrf_field() }}

            <div class="box box-info boxCriarPartida">


                <div class="box-header with-border">
                    <h3 class="box-title">Cadastro de Ordens de Serviço</h3>
                </div>



                <div id="status"></div>


                <div class="box-body">
                    <form role="form"> 
                        <div class="row">
                            <div class="col-sm-6">
                <div class="form-group">
                    <label for="cliente_id">Cliente:</label>
                    <select class="form-control" id="cliente_id" name="cliente_id">
                        <option value=""></option>

                        @foreach($clientes_pfs as $cliente_pf)    
                        <option value="{{$cliente_pf->id}}">{{$cliente_pf->nome_cliente}}</option>
                        @endforeach    
                    </select>
                </div>


 <div class="form-group">
                    <label for="funcionario_id">Funcionário Responsável:</label>
                    <select class="form-control" id="funcionario_id" name="funcionario_id">
                        <option value=""></option>

                        @foreach($funcionarios as $funcionario)    
                        <option value="{{$funcionario->id}}">{{$funcionario->nome}}</option>
                        @endforeach    
                    </select>
                </div>


            </div>





                            <div class="form-group col-md-6">

                                <label for="data">Data:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="date" class="form-control" id="data" name="data"  required>
                                </div>
                            </div>

                        </div>



                        <div class="row">



                            <div class="form-group col-md-6">
                                <label for="hora">Hora:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="time" class="form-control" id="hora" name="hora" required>
                                </div>
                            </div>



                            <div class="form-group col-md-6">
                                <label for="problema_relatado">Problema Relatado:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-futbol-o"></i>
                                    </div>
                                    <input type="text" class="form-control" id="problema_relatado" name="problema_relatado" required>
                                </div>
                            </div>


                        </div>
<!--
                        <div class="row">
                            
                            <div class="form-group col-md-6">
                                <label for="tipo_equipamento">Tipo de Equipamento: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-futbol-o"></i>
                                    </div>
                                    <input type="text" class="form-control" id="tipo_equipamento" name="tipo_equipamento" required>
                                </div>
                            </div>
                        </div>
-->
                  

                        <div class="row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-block btn-success">Enviar</button>
                            </div>
                            <div class="col-sm-6">
                                <button type="reset" class="btn btn-block btn-warning">Limpar</button>
                            </div>
                        </div>

                    </form>

                </div>

            </div>
        </form>
    </div>
</div>
</form>
</div>
</div>

@endsection