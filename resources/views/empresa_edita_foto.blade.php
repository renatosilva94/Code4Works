@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Foto
            <small>Cadastro e Alteração de Foto de Perfil</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Foto</li>
            <li class="active">Cadastro e Alteração de Foto de Perfil</li>
        </ol>   
    </section>    

    <div class="col-sm-12">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif   

        <form action="{{route('empresa.salva.foto.editada')}}" method="post" enctype="multipart/form-data">

            {{ csrf_field() }}
            
                <input type="hidden" name="id" value="{{$empresaId->id}}">

            </div>  
    

    
    <div class="col-sm-3" style="text-align: center">
@php        
    if(file_exists(public_path('fotos/'. $empresaId->id.'.jpg'))){
       $foto = '../fotos/'.$empresaId->id.'.jpg';
    } else {
       $foto = '../fotos/sem_foto.png';    
    }     
@endphp 
{!!"<img src=$foto id='imagem' width='150' height='130' alt='Foto' class='img-circle'>"!!}
        <div class="form-group">
            <input type="file" id="foto" name="foto" 
                   onchange="previewFile()"
                   class="form-control">
        </div>
    </div>
    

    
            <button type="submit" class="btn btn-primary btn-sm">Salvar Foto</button>        

</div>
</form>

<script>
function previewFile() {
    var preview = document.getElementById('imagem');
    var file    = document.getElementById('foto').files[0];
    var reader  = new FileReader();
    
    reader.onloadend = function() {
        preview.src = reader.result;
    };
    
    if (file) {
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }    
}

$(document).ready(function () {
    $('#preco').mask("#.###.##0,00", {reverse: true});
});
</script>    

@endsection
