<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Code4Works | Dashboard</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
       <script src="{{asset('https://code.jquery.com/jquery-3.1.1.min.js')}}"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
               <script src="{{asset('js/jquery.min.js')}}"></script>

        
        <link rel="stylesheet" href="{{ asset('css/dashboard/font-awesome.min.css') }}">
        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="{{ asset('css/dashboard/AdminLTE.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/dashboard/skins/_all-skins.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/dashboard/morris.css') }}">
        <link rel="stylesheet" href="{{ asset('css/dashboard/jquery-jvectormap.css') }}">
        <link rel="stylesheet" href="{{ asset('css/dashboard/bootstrap-datepicker.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/dashboard/daterangepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('css/dashboard/bootstrap3-wysihtml5.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/empresa_dashboard.css') }} ">
        <link rel="stylesheet" href="{{ asset('https://fonts.googleapis.com/css?family=Bree+Serif') }} ">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        
            <script>

        $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

    </script>
    
    </head>
    <body class="hold-transition skin-blue sidebar-mini" style="height: 100vh;">

        <div class="wrapper">

            <header class="main-header">
                <a href="{{route('empresa.dashboard.principal')}}" class="logo">
                    <span class="logo-mini"><b>C</b>W</span>
                    <span class="logo-lg"><b>Code4Works</b></span>
                </a>
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">


                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    @php        
                                    if(file_exists(public_path('fotos/'.$usuario_autenticado_id.'.jpg'))){
                                    $foto = '../fotos/'.$usuario_autenticado_id.'.jpg';
                                    } else {
                                    $foto = '../fotos/sem_foto.png';    
                                    }     
                                    @endphp 
                                    {!!"<img src=$foto id='imagem' width='200' height='180' alt='Foto' class='user-image'>"!!}
                                    <span class="hidden-xs">{{$usuario_autenticado_nome}}</span>
                                </a>

                                <ul class="dropdown-menu">
                                   <li class="user-header">
                                        @php        
                                        if(file_exists(public_path('fotos/'.$usuario_autenticado_id.'.jpg'))){
                                        $foto = '../fotos/'.$usuario_autenticado_id.'.jpg';
                                        } else {
                                        $foto = '../fotos/sem_foto.png';    
                                        }     
                                        @endphp 
                                        {!!"<img src=$foto id='imagem' width='160' height='160' alt='Foto' class='img-circle'>"!!}
                                        <p>
                                            Usuário Code4Works - Empresa
                                            <small>Member since Nov. 2012</small>
                                        </p>
                                    </li>


                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="main-sidebar">
                <section class="sidebar">
                    <div class="user-panel">

                         <div class="pull-left image">
                            @php        
                            if(file_exists(public_path('fotos/'.$usuario_autenticado_id.'.jpg'))){
                            $foto = '../fotos/'.$usuario_autenticado_id.'.jpg';
                            } else {
                            $foto = '../fotos/sem_foto.png';    
                            }     
                            @endphp 
                            {!!"<img src=$foto id='imagem' alt='Foto' class='img-circle'>"!!}
                        </div>

                        <div class="pull-left info nomeUsuario">
                            <p>{{$usuario_autenticado_nome}}</p>
                        </div>
                    </div>
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MENU DE NAVEGAÇÃO</li>
                        <li class="active treeview">
                            <a href="{{route('empresa.dashboard.principal')}}">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>





                    <!-- Gestão de Clientes -->

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-address-card "></i>
                                <span>Cadastros</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                  <li><a href="{{route('empresa.lista.cliente.pf')}}"><i class="fa fa-circle-o"></i>Pessoa Física</a></li>
                                    <li><a href="{{route('empresa.lista.cliente.pj')}}"><i class="fa fa-circle-o"></i>Pessoa Juridica</a></li>
                                    <li><a href="{{route('empresa.lista.fornecedor')}}"><i class="fa fa-circle-o"></i>Fornecedores</a></li>
                                

                                
                            </ul>
                            </a>
                        </li>

                        <!-- Fim do Menu de Gestão de Clientes -->


                        <!-- Quebra de Linha -->


                        <!-- Gerenciamento de Fornecedores -->

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-address-card "></i>
                                <span>Relacionamentos</span>
                                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>


                                </span>
                            </a>
                            <ul class="treeview-menu">
                                  <li><a href="{{route('empresa.lista.agenda')}}"><i class="fa fa-circle-o"></i>Agenda</a></li>
                                  <li><a href="#"><i class="fa fa-circle-o"></i>Pendencias</a></li>

                            </ul>
                            </a>
                        </li>

                        <!-- Fim do Menu de Gerenciamento de Fornecedores -->

                        <!-- Quebra de Linha -->

                        <!-- Ordem de Serviço -->

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-signal"></i>
                                <span>Serviços</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{route('empresa.lista.ordem.servico.pf')}}"><i class="fa fa-circle-o"></i>Ordens de Serviço PF</a></li>
                                 <li><a href="{{route('empresa.lista.ordem.servico.pj')}}"><i class="fa fa-circle-o"></i>Ordens de Serviço PJ</a></li>
                                

                            </ul>
                        </li>

                        <!-- Fim do Menu de Ordem de Serviço -->

                        <!-- Recursos Humanos -->

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-address-card "></i>
                                <span>Recursos Humanos</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{route('empresa.lista.funcionario')}}"><i class="fa fa-circle-o"></i>Funcionários</a></li>
                                <li><a href="{{route('empresa.lista.cargo.salario')}}"><i class="fa fa-circle-o"></i>Cargos e Salários</a></li>
                                
                                
                                
                            </ul>
                            </a>
                        </li>



                        <!-- Fim do Menu de Recursos Humanos -->




                        <!-- Vendas -->


                            <li class="treeview">
                            <a href="#">
                                <i class="fa fa-address-card "></i>
                                <span>Vendas</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                  <li><a href="{{route('empresa.lista.orcamento.pf')}}"><i class="fa fa-circle-o"></i>Orçamentos PF</a></li>
                                  <li><a href="#"><i class="fa fa-circle-o"></i>Pedidos</a></li>
                                  <li><a href="#"><i class="fa fa-circle-o"></i>Metas</a></li>                                
                            </ul>
                            </a>
                        </li>





                        <!-- Fim do Menu de Vendas -->






                        <!-- Compras -->



 <li class="treeview">
                            <a href="#">
                                <i class="fa fa-address-card "></i>
                                <span>Compras</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                  <li><a href="{{route('empresa.lista.compra')}}"><i class="fa fa-circle-o"></i>Lista de Compras</a></li>
                                  <li><a href="#"><i class="fa fa-circle-o"></i>Contas Fixas</a></li>
                                  <li><a href="#"><i class="fa fa-circle-o"></i>Contas do Mês</a></li>
                                
                            </ul>
                            </a>
                        </li>



<!-- Fim do Menu de Compras -->



<!-- Estoque -->

<li class="treeview">
                            <a href="#">
                                <i class="fa fa-address-card "></i>
                                <span>Estoque</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                  <li><a href="{{route('empresa.lista.produto')}}"><i class="fa fa-circle-o"></i>Lista de Produtos</a></li>
                                  
                                
                            </ul>
                            </a>
                        </li>










































                        <!-- EM CONSTRUÇãO -->















                       
                        
                        
                        
                        
                        
                        
                        






                        


                        
                        
                        
                        
                        
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-cogs"></i>
                                <span>Configurações</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">

                                <li><a href="{{route('empresa.edita.perfil', $usuario_autenticado_id)}}"><i class="fa fa-circle-o"></i>Editar Perfil</a></li>
                                <li><a href="{{route('empresa.edita.foto', $usuario_autenticado_id)}}"><i class="fa fa-circle-o"></i>Editar Foto</a></li>
                                <li><a href="{{route('empresa.efetuar.logout')}}"><i class="fa fa-circle-o"></i>Sair</a></li>


                            </ul>
                        </li>
                    </ul>
                </section>
            </aside>
            <div class="conteudo">
                @yield('conteudo')
            </div>

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
                <strong>Copyright &copy; 2017 <a href="/login">Code4Works</a>.</strong> Todos os direitos reservados.
            </footer>

            <div class="control-sidebar-bg"></div>
        </div>
        <script src="{{ asset('js/dashboard/jquery-3.2.1.min.js') }}"></script>
        <script src="{{ asset('js/dashboard/jquery-ui.min.js') }}"></script>
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <script src="{{ asset('js/dashboard/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/dashboard/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('js/dashboard/raphael.min.js') }}"></script>
        <script src="{{ asset('js/dashboard/morris.min.js') }}"></script>
        <script src="{{ asset('js/dashboard/jquery.sparkline.min.js') }}"></script>
        <script src="{{ asset('js/dashboard/jquery-jvectormap-1.2.2.min.js') }}"></script>
        <script src="{{ asset('js/dashboard/jquery-jvectormap-world-mill-en.js') }}"></script>
        <script src="{{ asset('js/dashboard/dataTables.bootstrap.min.js') }} "></script>
        <script src="{{ asset('js/dashboard/jquery.knob.min.js') }}"></script>
        <script src="{{ asset('js/dashboard/moment.min.js') }}"></script>
        <script src="{{ asset('js/dashboard/daterangepicker.js') }}"></script>
        <script src="{{ asset('js/dashboard/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('js/dashboard/bootstrap3-wysihtml5.all.min.js') }}"></script>
        <script src="{{ asset('js/dashboard/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('js/dashboard/fastclick.js') }}"></script>
        <script src="{{ asset('js/dashboard/adminlte.min.js') }}"></script>
        <script src="{{ asset('js/dashboard/dashboard.js') }}"></script>
        <script src="{{ asset('js/dashboard/demo.js') }}"></script>


    </body>
</html>