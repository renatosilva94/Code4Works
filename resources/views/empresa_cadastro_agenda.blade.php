@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Agenda
            <small>Cadastro de Aviso</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Relacionamentos</li>
            <li class="active">Mural</li>
        </ol>   
    </section>    

    <div class="col-sm-12 boxCenterPartida">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif   

        <form action="#" method="post">



            {{ csrf_field() }}

            <div class="box box-info boxCriarPartida">


                <div class="box-header with-border">
                    <h3 class="box-title">Cadastro de Avisos</h3>
                </div>



                <div id="status"></div>


                <div class="box-body">
                    <form role="form"> 
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="titulo">Título: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-users"></i>
                                    </div>
                                    <input type="text" class="form-control" id="titulo" name="titulo" value="" required>
                                </div>
                            </div>



                            <div class="form-group col-md-6">

                                <label for="assunto">Assunto:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="text" class="form-control" id="assunto" name="assunto" value="" required>
                                </div>
                            </div>

                        </div>



                        <div class="row">



                            <div class="form-group col-sm-6">
                                <label for="tipo_conteudo">Tipo de Conteudo: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-university"></i>
                                    </div>
                                    <select class="form-control" id="tipo_conteudo" name="tipo_conteudo">
                                        <option></option>
                                        @foreach($tipos_conteudos as $tipo_conteudo)
                                        <option value="{{$tipo_conteudo->id}}">{{$tipo_conteudo->nome_tipo_conteudo}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            
                            <div class="form-group col-sm-6">
                                <label for="prioridade">Prioridade: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-university"></i>
                                    </div>
                                    <select class="form-control" id="prioridade" name="prioridade">
                                        <option></option>
                                        @foreach($prioridades as $prioridade)
                                        <option value="{{$prioridade->id}}">{{$prioridade->nome_prioridade}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                        </div>

                        



                        <script>
                            $('#estado_id').on('change', function () {
                                var estadoID = $(this).val();
                                if (estadoID) {
                                    $.ajax({
                                        type: "GET",
                                        url: "{{url('ajax/pegar-lista-cidades')}}?estado_id=" + estadoID,
                                        success: function (res) {
                                            if (res) {
                                                $("#cidade_id").empty();
                                                $.each(res, function (key, value) {
                                                    $("#cidade_id").append('<option value="' + key + '">' + value + '</option>');
                                                });

                                            } else {
                                                $("#cidade_id").empty();
                                            }
                                        }
                                    });
                                } else {
                                    $("#cidade_id").empty();
                                }

                            });

                        </script>
                        


                        <div class="row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-block btn-success">Enviar</button>
                            </div>
                            <div class="col-sm-6">
                                <button type="reset" class="btn btn-block btn-warning">Limpar</button>
                            </div>
                        </div>

                    </form>

                </div>

            </div>
        </form>
    </div>
</div>
</form>
</div>
</div>

@endsection