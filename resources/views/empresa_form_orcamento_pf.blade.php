@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Orçamento
            <small>Cadastro de Orçamento - PF</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Orçamento</li>
            <li class="active">Cadastro de Orçamento - PF</li>
        </ol>   
    </section>    

    <div class="col-sm-12 boxCenterPartida">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif   

        @if($acao == 1)
        <form action="{{route('empresa.cadastra.orcamento.pf')}}" method="post">
        @else
        <form action="#" method="post">
        @endif


            {{ csrf_field() }}

            <div class="box box-info boxCriarPartida">


                <div class="box-header with-border">
                    <h3 class="box-title">Cadastro de Orçamento PF</h3>
                </div>



                <div id="status"></div>


                <div class="box-body">
                     <div id="sections">
             


<fieldset>
                       
                        <div>

                            <div class="form-group col-sm-6">
                                    <label for="cliente_id">Cliente:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-map-signs"></i>
                                        </div>
                                        <select class="form-control" id="cliente_id" name="cliente_id">
                                            <option></option>
                                            @foreach($clientes_pfs as $cliente_pf)
                                            <option value="{{$cliente_pf->id}}">{{$cliente_pf->nome_cliente}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


         <div class="form-group col-md-6">
                                <label for="nome_orcacmento">Nome do Orçamento:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="text" class="form-control" id="nome_orcamento" name="nome_orcamento"/>
                                </div>
                            </div>










 <div class="section">


                            <div class="form-group col-sm-3">
                                    <label for="produto_id">Produto:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-map-signs"></i>
                                        </div>
                                        <select class="form-control" id="produto_id" name="produto_id[]">
                                            <option></option>
                                            @foreach($produtos as $produto)
                                            <option value="{{$produto->id}}">{{$produto->nome_produto}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                <label for="valor_unitario">Preço do Produto:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="number" class="form-control" id="valor_unitario" name="valor_unitario[]"/>
                                </div>
                            </div>


                            <div class="form-group col-md-3">
                                <label for="quantidade">Quantidade:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="number" class="form-control" id="quantidade" name="quantidade[]">
                                </div>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="valor_total">Preço Total:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="number" class="form-control" id="valor_total" name="valor_total[]"/>
                                </div>
                            </div>





                       
<!-- Botão para Remover Formulário -->
                        <div>
                            <div class="col-lg-12">
                                <button type="" class="remove btn btn-block btn-info btn-sm">Remover Formulário</button>
                            </div>
                         </div>

                        </div>

                 

                        
                            

                </div>
            </div>
                            <!-- -->
                        


                     </fieldset>
                 </div>


<!-- Botão para Adicionar Formulário -->
  
                            <!-- -->
</div>
<div class="col-sm-3">
                                <button type="" class="addsection btn btn-block btn-yahoo btn-sm">Adicionar Formulário</button>
                            </div>

<div class="row">
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-block btn-success btn-sm">Salvar</button>
                            </div>
                            <div class="col-sm-3">
                                <button type="reset" class="btn btn-block btn-warning btn-sm">Limpar</button>
                            </div>


                    </form>

<!-- Inicio do Script de Adição de Formulário -->

<script>
    


    //define template
var template = $('#sections .section:first').clone();

//define counter
var sectionsCount = 1;


//add new section
$('body').on('click', '.addsection', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

        //set id to store the updated section number
        var newId = this.id + sectionsCount;

        //update for label
        $(this).prev().attr('for', newId);

        //update id
        this.id = newId;

    }).end()

    //inject new section
    .appendTo('#sections');
    return false;
});

//remove section
$('#sections').on('click', '.remove', function() {
    //fade out section
    $(this).parent().fadeOut(300, function(){
        //remove parent element (main section)
        $(this).parent().parent().empty();
        return false;
    });
    return false;
});
</script>
<!-- Fim do Script -->

                </div>

            </div>
        </form>
    </div>
</div>
</form>
</div>
</div>

@endsection