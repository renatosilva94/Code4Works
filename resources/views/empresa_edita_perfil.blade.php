@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Usuário
            <small>Editar Perfil</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Usuário</li>
            <li class="active">Editar Perfil</li>
        </ol>   
    </section>

    <form action="{{route('empresa.salva.perfil.editado', $usuarioId->id)}}" method="post">
        {{ csrf_field() }}

        
         <div class="box-body">
                    <form role="form"> 
                        <div class="form-group">
                            <label for="nome">Nome :</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-commenting"></i>
                                </div>
                                <input type="text" class="form-control" id="nome" name="nome" value="{{$usuarioId->nome or old('nome')}}" required>
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <label for="sobrenome">Sobrenome :</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-commenting"></i>
                                </div>
                                <input type="text" class="form-control" id="sobrenome" name="sobrenome" value="{{$usuarioId->sobrenome or old('sobrenome')}}" required>
                            </div>
                        </div>
                             
                             
                             
                        <div class="form-group">
                            <label for="email">Email :</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-commenting"></i>
                                </div>
                                <input type="text" class="form-control" id="email" name="email" value="{{$usuarioId->email or old('email')}}" required>
                            </div>
                        </div>

                             <div class="form-group">
                            <label for="password">Senha :</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-commenting"></i>
                                </div>
                                <input type="password" class="form-control" id="password" name="password">
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
            <label for="sexo">Sexo:</label>

            <select class="form-control" id="sexo" name="sexo">

                <option value="M"
                        @if ((isset($usuarioId) and $usuarioId->sexo == "Masculino") or old('sexo') == "M") selected @endif                        
                        >Masculino</option>
                <option value="F"
                        @if ((isset($usuarioId) and $usuarioId->sexo == "Feminino") or old('sexo') == "F") selected @endif                                                
                        >Feminino</option>
                <option value="O"
                        @if ((isset($usuarioId) and $usuarioId->sexo == "Outro") or old('sexo') == "O") selected @endif                                                
                        >Outro</option>
                
                
            </select>
        </div>
                        
                        <div class="form-group">
                            <label for="nome">Telefone :</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-commenting"></i>
                                </div>
                                <input type="number" class="form-control" id="telefone" name="telefone" value="{{$usuarioId->telefone}}" required>
                            </div>
                        </div>
                        
                        
              
        <button type="submit" class="btn btn-primary">Enviar</button>
        <button type="reset" class="btn btn-warning">Limpar</button>
          
                        

                             
                        
         </div>
                        
                        
      
        


        
    </form>

    
@endsection
