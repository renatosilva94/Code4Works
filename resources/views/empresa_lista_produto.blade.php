@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Produtos
            <small>Lista de Produtos</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Produtos</li>
            <li class="active">Lista de Produtos</li>
        </ol>   
    </section>

    <form method="post" action="#">
        {{ csrf_field() }}

        <div class="selectSearch">
          

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="cidade_id">Cidade:</label>
                    <select class="form-control" id="cidade_id" name="cidade_id">
                        <option value=""></option>

                        @foreach($cidades as $cidade)    
                        <option value="{{$cidade->id}}">{{$cidade->nome_cidade}}</option>
                        @endforeach    
                    </select>
                </div>
            </div>


            <div class='col-sm-6 btnPesquisar'>
                <button type="submit" class="btn btn-bitbucket">Pesquisar</button>  
                <a href="{{route('empresa.cadastro.produto')}}" class="btn btn-outline btn-success">Cadastro de Produto</a>            
          
            </div>
        </div>
    </form>

    <div class='col-sm-12'>

         @if (count($produtos)==0)
        <div class="alert alert-danger">
            Não Existem Produtos Com O Filtro Informado
        </div>
         @endif
    </div>

    <div class="col-sm-12 boxCenter boxCentralize">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Lista de Produtos</h3>
                </div>
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="thListagem">Nome do Produto</th>
                                <th class="thListagem">Quantidade em Estoque</th>
                                <th class="thListagem">Código de Barras</th>
                                <th class="thListagem">Data de Compra</th>             

                                <th class="thListagem">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($produtos as $produto)

                            <tr>

                                <td class="tdListagem">{{$produto->nome_produto}}</td>
                                <td class="tdListagem">{{$produto->quantidade_estoque}}</td>
                                <td class="tdListagem">{{$produto->codigo_barras}}</td>
                                <td class="tdListagem">{{$produto->data_compra}}</td>
                                

                                <td>
                                   

                                     <a href="{{route('empresa.edita.produto', $produto->id)}}" 
                                       class="btn btn-success btn-sm" 
                                       role="button">Editar
                                    </a>

                                    <form style="display: inline-block"
                          method="post"
                          action="{{route('empresa.deleta.produto', $produto->id)}}"
                          onsubmit="return confirm('Confirma Exclusão do Produto ?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger btn-sm"> Excluir </button>
                    </form> 


                                </td>
                                
                                @endforeach
                                
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>  
        </div>
    </div>
</div>
@endsection