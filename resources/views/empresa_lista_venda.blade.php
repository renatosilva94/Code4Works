@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Produtos
            <small>Lista de Vendas</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Vendas</li>
            <li class="active">Lista de Vendas</li>
        </ol>   
    </section>

    <form method="post" action="#">
        {{ csrf_field() }}

        <div class="selectSearch">
          

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="cidade_id">Cidade:</label>
                    <select class="form-control" id="cidade_id" name="cidade_id">
                        <option value=""></option>

                        @foreach($cidades as $cidade)    
                        <option value="{{$cidade->id}}">{{$cidade->nome_cidade}}</option>
                        @endforeach    
                    </select>
                </div>
            </div>


            <div class='col-sm-1 btnPesquisar'>
                <button type="submit" class="btn btn-bitbucket">Pesquisar</button>            
            </div>
        </div>
    </form>

    <div class='col-sm-12'>

         @if (count($produtos)==0)
        <div class="alert alert-danger">
            Não Existem Vendas Com O Filtro Informado
        </div>
         @endif
    </div>

    <div class="col-sm-12 boxCenter boxCentralize">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Lista de Vendas</h3>
                </div>
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="thListagem">Nome do Produto</th>
                                <th class="thListagem">Total Vendido</th>
                                <th class="thListagem">Valor Total</th>
                                <th class="thListagem">Data de Venda</th>             

                                <th class="thListagem">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($vendas as $venda)

                            <tr>

                                <td class="tdListagem"></td>
                                <td class="tdListagem"></td>
                                <td class="tdListagem"></td>
                                <td class="tdListagem"></td>
                                

                                <td>
                                   

                                     

                                    <form style="display: inline-block"
                          method="post"
                          action="#"
                          onsubmit="return confirm('Confirma Exclusão da Venda ?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger btn-sm"> Excluir </button>
                    </form> 


                                </td>
                                
                                @endforeach
                                
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>  
        </div>
    </div>
</div>
@endsection