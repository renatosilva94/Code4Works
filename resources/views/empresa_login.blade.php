<!DOCTYPE html>
<html lang="br">
    <head>
        <meta charset="utf-8">
        <title>Code4Works - Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="{{ asset('css/empresa_login.css') }}">
    </head>
    <body>
        <div class="container" >  
            <div class="col-lg-4 col-md-3 col-sm-2"></div>
            <div class="col-lg-4 col-md-6 col-sm-8">


                <div class="row loginbox">                    
                    <div class="col-lg-12">
                        <center><img class="loginAvatar" src="/img/avatar.png"></center>
                        <span class="singtext" >Login</span>  
                    </div>

                    <form method="POST" role="form" action="{{ route('empresa.login.efetuar') }}">
                        {{ csrf_field() }}


                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <input class="form-control" type="email" placeholder="Digite seu Email" name="email" id="email" > 
                        </div>
                        <div class="col-lg-12  col-md-12 col-sm-12">
                            <input class="form-control" type="password" placeholder="Digite sua Senha" name="password" id="password">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </div>

                        <div class="col-lg-12  col-md-12 col-sm-12">
                            <button class="btn submitButton" type="submit">Entrar</button>&nbsp;

                            
                        </div>  
                    </form>


                                                <div class="col-lg-12  col-md-6 col-sm-6">

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-success" href="{{route('empresa.cadastro')}}">Cadastre-se</a>&nbsp;
                            <a class="btn btn-danger" href="#">Esqueceu a Senha ?</a>&nbsp;

                        </div>








                       


                        

                </div>
                <br>                
                <br>
            </div>
            <div class="col-lg-4 col-md-3 col-sm-2"></div>
        </div>
    </body>
</html>