@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Produtos
            <small>Cadastro de Produto</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Produtos</li>
            <li class="active">Cadastro de Produtos</li>
        </ol>   
    </section>    

    <div class="col-sm-12 boxCenterPartida">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif   

        @if($acao == 1)
        <form action="{{route('empresa.cadastra.produto')}}" method="post">
        @else
        <form action="{{route('empresa.salva.produto.editado', $produtoId->id)}}" method="post">
        @endif


            {{ csrf_field() }}

            <div class="box box-info boxCriarPartida">


                <div class="box-header with-border">
                    <h3 class="box-title">Cadastro de Produto</h3>
                </div>



                <div id="status"></div>


                <div class="box-body">
                    <form role="form"> 
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="nome_produto">Nome do Produto: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-users"></i>
                                    </div>
                                    <input type="text" class="form-control" id="nome_produto" name="nome_produto" value="{{$produtoId->nome_produto or old('nome_produto')}}" required>
                                </div>
                            </div>



                            <div class="form-group col-md-6">

                                <label for="quantidade_estoque">Quantidade em Estoque:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="text" class="form-control" id="quantidade_estoque" name="quantidade_estoque" value="{{$produtoId->quantidade_estoque or old('quantidade_estoque')}}" required>
                                </div>
                            </div>

                        </div>



                        <div class="row">



                            <div class="form-group col-md-6">
                                <label for="codigo_barras">Código de Barras:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="text" class="form-control" id="codigo_barras" name="codigo_barras" value="{{$produtoId->codigo_barras or old('codigo_barras')}}" required>
                                </div>
                            </div>



                            <div class="form-group col-md-6">
                                <label for="data_compra">Data de Compra:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-futbol-o"></i>
                                    </div>
                                    <input type="date" class="form-control" id="data_compra" name="data_compra" value="{{$produtoId->data_compra or old('data_compra')}}" required>
                                </div>
                            </div>


                        </div>

                  

                        <div class="row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-block btn-success">Enviar</button>
                            </div>
                            <div class="col-sm-6">
                                <button type="reset" class="btn btn-block btn-warning">Limpar</button>
                            </div>
                        </div>

                    </form>

                </div>

            </div>
        </form>
    </div>
</div>
</form>
</div>
</div>

@endsection