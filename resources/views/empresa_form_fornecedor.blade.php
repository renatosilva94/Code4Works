@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Fornecedores
            <small>Cadastro de Fornecedor</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Fornecedores</li>
            <li class="active">Cadastro de Fornecedores</li>
        </ol>   
    </section>    

    <div class="col-sm-12 boxCenterPartida">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif   

        <form action="{{route('empresa.cadastra.fornecedor')}}" method="post">



            {{ csrf_field() }}

            <div class="box box-info boxCriarPartida">


                <div class="box-header with-border">
                    <h3 class="box-title">Cadastro de Fornecedores</h3>
                </div>



                <div id="status"></div>


                <div class="box-body">
                    <form role="form"> 
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="cnpj">CNPJ: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-users"></i>
                                    </div>
                                    <input type="text" class="form-control" id="cnpj" name="cnpj" value="{{$fornecedor->cnpj or old('cnpj')}}" required>
                                </div>
                            </div>



                            <div class="form-group col-md-6">

                                <label for="nome_fantasia">Nome Fantasia:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="text" class="form-control" id="nome_fantasia" name="nome_fantasia" value="{{$fornecedor->nome_fantasia or old('nome_fantasia')}}" required>
                                </div>
                            </div>

                        </div>



                        <div class="row">



                            <div class="form-group col-md-6">
                                <label for="razao_social">Razão Social:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="text" class="form-control" id="razao_social" name="razao_social" value="{{$fornecedor->razao_social or old('razao_social')}}" required>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="telefone">Telefone para Contato:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-futbol-o"></i>
                                    </div>
                                    <input type="number" class="form-control" id="telefone" name="telefone" value="{{$fornecedor->telefone or old('telefone')}}" required>
                                </div>
                            </div>


                        </div>

                        <div class="row">

                            <div class="form-group col-sm-6">
                                <label for="estado_id">Estado: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-university"></i>
                                    </div>
                                    <select class="form-control" id="estado_id" name="estado_id">
                                        <option></option>
                                        @foreach($estados as $estado)
                                        <option value="{{$estado->id}}">{{$estado->nome_estado}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="cidade_id">Cidade:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-map-signs"></i>
                                    </div>
                                    <select class="form-control" id="cidade_id" name="cidade_id">
                                        <option></option>
                                        @foreach($cidades as $cidade)
                                        <option value="{{$cidade->id}}">{{$cidade->nome_cidade}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>




                        <script>
                            $('#estado_id').on('change', function () {
                                var estadoID = $(this).val();
                                if (estadoID) {
                                    $.ajax({
                                        type: "GET",
                                        url: "{{url('ajax/pegar-lista-cidades')}}?estado_id=" + estadoID,
                                        success: function (res) {
                                            if (res) {
                                                $("#cidade_id").empty();
                                                $.each(res, function (key, value) {
                                                    $("#cidade_id").append('<option value="' + key + '">' + value + '</option>');
                                                });

                                            } else {
                                                $("#cidade_id").empty();
                                            }
                                        }
                                    });
                                } else {
                                    $("#cidade_id").empty();
                                }

                            });

                        </script>
                        <div class="row">


                            <div class="form-group col-sm-6">
                                <label>CEP:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <input type="text" class="form-control" id="cep" name="cep" value="{{$fornecedor->cep or old('cep')}}" required>
                                </div>
                            </div>





                            <div class="form-group col-sm-6">
                                <label>Bairro:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-map-pin"></i>
                                    </div>
                                    <input type="text" class="form-control" id="bairro" name="bairro" value="{{$fornecedor->bairro or old('bairro')}}" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="form-group col-sm-6">
                                <label>Rua:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <input type="text" class="form-control" id="rua" name="rua" value="{{$fornecedor->rua or old('rua')}}" required>
                                </div>
                            </div>

                            <div class="form-group col-sm-6">
                                <label>Número:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <input type="text" class="form-control" id="numero" name="numero" value="{{$fornecedor->numero or old('numero')}}" required>
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="form-group col-sm-6">
                                <label>Email :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <input type="email" class="form-control" id="email" name="email" value="{{$fornecedor->email or old('email')}}" required>
                                </div>
                            </div>

                        </div>









                        <div class="row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-block btn-success">Enviar</button>
                            </div>
                            <div class="col-sm-6">
                                <button type="reset" class="btn btn-block btn-warning">Limpar</button>
                            </div>
                        </div>

                    </form>

                </div>

            </div>
        </form>
    </div>
</div>
</form>
</div>
</div>

@endsection