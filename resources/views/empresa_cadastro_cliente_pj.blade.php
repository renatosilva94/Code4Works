@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Clientes
            <small>Cadastro de Cliente PJ</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Clientes</li>
            <li class="active">Cadastro de Cliente PJ</li>
        </ol>   
    </section>    

    <div class="col-sm-12 boxCenterPartida">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif   

        @if($acao == 1)
        <form action="{{route('empresa.cadastra.cliente.pj')}}" method="post">
        @else
                <form action="{{route('empresa.salva.cliente.pj.editado', $clientePjId->id)}}" method="post">
                @endif

            {{ csrf_field() }}

            <div class="box box-info boxCriarPartida">


                <div class="box-header with-border">
                    <h3 class="box-title">Cadastro de Cliente - Pessoa Juridica</h3>
                </div>



                <div id="status"></div>

 <script>

/**
 * Created by rafaelcaviquioli <rafaelcitj@gmail.com>
 * User: rafaelcaviquioli
 * Date: 26/01/17
 * Time: 22:31
 */
(function ($) {
    $.fn.receitaws = function (options) {
        $this = this;

        $.fn.receitaws.options = {
            afterRequest: function () {
            },
            success: function (data) {
            },
            notfound: function (message) {
            },
            fail: function (message) {
            },
            fields: {},
            urlRequest: '../public/php/receita-ws.php'
        };

        /*
         Duplicate request controller. (Cache)
         */
        $.fn.receitaws.lastRequest = {
            cnpj: null,
            data: null
        };


        function getData(cnpj) {
            cnpj = cnpj.replace(/\D/g, '');

            var lastRequest = $.fn.receitaws.lastRequest;

            return new Promise(function (success, fail) {
                if (lastRequest.cnpj == cnpj) {
                    success(lastRequest.dados);
                } else {
                    $.getJSON($.fn.receitaws.options.urlRequest + "?cnpj=" + cnpj, function (data) {
                        lastRequest.cnpj = cnpj;
                        lastRequest.dados = data;

                        success(data);
                    }).fail(function (jqxhr, textStatus, error) {
                        fail(textStatus + ", " + error);
                    });
                }
            });
        }


        $.fn.receitaws.init = function (options) {

            $.fn.receitaws.options = $.extend({}, $.fn.receitaws.options, options);

            return $this.keyup(function () {
                var cnpj = $(this).val().replace(/\D/g, '');

                if (cnpj.length != 14) {
                    return false;
                }
                options.afterRequest();

                getData(cnpj).then(function (data) {

                    if (data.status == 'OK') {
                        $.each(options.fields, function (index, value) {
                            if (typeof value == "string") {
                                $(options.fields[index]).val(data[index]);
                            } else if (typeof value == 'function') {
                                value(data[index]);
                            }
                        });

                        options.success(data);
                    } else {
                        options.notfound('CNPJ "' + $this.val() + '" not found.');
                    }

                }, function (error) {
                    options.fail(error);
                });
            });
        };

        return $.fn.receitaws.init(options);
    };
})(jQuery);






















 $(document).ready(function () {
        function fecharMensagem() {
            setTimeout(function () {
                $('#status').html('');
                $('#cnpj').prop('disabled', false);
            }, 2000);
        }
        $('#cnpj').receitaws({
            afterRequest: function () {
                var cnpj = $('#cnpj').val();
                $('#status').html('<div class="alert alert-info">Buscando CNPJ</div>');
                $('form').find("input[type=text]").val("");
                $('#cnpj').val(cnpj);
                $('#cnpj').prop('disabled', true);
            },
            success: function (data) {
                $('#status').html('<div class="alert alert-success">CNPJ Encontrado</div>');
                fecharMensagem();
            },
            fail: function (message) {
                $('#status').html('<div class="alert alert-danger">' + message + '</div>');
                fecharMensagem();
            },
            notfound: function (message) {
                $('#status').html('<div class="alert alert-warning">CNPJ inexistente</div>');
                fecharMensagem();
            },
            fields: {
                nome: '#razao_social',
                fantasia: '#nome_fantasia',
                
                telefone: function (data) {
                    var separa = data.split('/');
                    if (typeof separa[0] != 'undefined') {
                        $('#fone1').val(separa[0]);
                    }
                },
                qsa: function (data) {
                    var responsaveis = [];
                    $.each(data, function(i, val) {
                        if (typeof val != 'undefined') {
                            responsaveis[i] = val.nome
                        }
                    });
                    $('#responsavel').val(responsaveis.join(','));
                }
            }
        });
    });
        </script>




                <div class="box-body">
                    <form role="form"> 
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="cnpj">CNPJ: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-users"></i>
                                    </div>
                                    <input type="text" class="form-control" id="cnpj" name="cnpj" value="{{$clientePjId->cnpj or old('cnpj')}}" required>
                                </div>
                            </div>



                            <div class="form-group col-md-6">

                                <label for="nome_fantasia">Nome Fantasia:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="text" class="form-control" id="nome_fantasia" name="nome_fantasia" value="{{$clientePjId->nome_fantasia or old('nome_fantasia')}}" required>
                                </div>
                            </div>

                        </div>



                        <div class="row">



                            <div class="form-group col-md-6">
                                <label for="razao_social">Razão Social:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="text" class="form-control" id="razao_social" name="razao_social" value="{{$clientePjId->razao_social or old('razao_social')}}" required>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="telefone">Telefone para Contato:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-futbol-o"></i>
                                    </div>
                                    <input type="number" class="form-control" id="telefone" name="telefone" value="{{$clientePjId->telefone or old('telefone')}}" required>
                                </div>
                            </div>


                        </div>

                        <div class="row">

                            <div class="form-group col-sm-6">
                                <label for="estado_id">Estado: </label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-university"></i>
                                    </div>
                                    <select class="form-control" id="estado_id" name="estado_id">
                                        <option></option>
                                        @foreach($estados as $estado)
                                        <option value="{{$estado->id}}">{{$estado->nome_estado}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="cidade_id">Cidade:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-map-signs"></i>
                                    </div>
                                    <select class="form-control" id="cidade_id" name="cidade_id">
                                        <option></option>
                                        @foreach($cidades as $cidade)
                                        <option value="{{$cidade->id}}">{{$cidade->nome_cidade}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>




                        <script>
                            $('#estado_id').on('change', function () {
                                var estadoID = $(this).val();
                                if (estadoID) {
                                    $.ajax({
                                        type: "GET",
                                        url: "{{url('ajax/pegar-lista-cidades')}}?estado_id=" + estadoID,
                                        success: function (res) {
                                            if (res) {
                                                $("#cidade_id").empty();
                                                $.each(res, function (key, value) {
                                                    $("#cidade_id").append('<option value="' + key + '">' + value + '</option>');
                                                });

                                            } else {
                                                $("#cidade_id").empty();
                                            }
                                        }
                                    });
                                } else {
                                    $("#cidade_id").empty();
                                }

                            });

                        </script>
                        <div class="row">


                            <div class="form-group col-sm-6">
                                <label>CEP:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <input type="text" class="form-control" id="cep" name="cep" value="{{$clientePjId->cep or old('cep')}}" required>
                                </div>
                            </div>





                            <div class="form-group col-sm-6">
                                <label>Bairro:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-map-pin"></i>
                                    </div>
                                    <input type="text" class="form-control" id="bairro" name="bairro" value="{{$clientePjId->bairro or old('bairro')}}" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="form-group col-sm-6">
                                <label>Rua:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <input type="text" class="form-control" id="rua" name="rua" value="{{$clientePjId->rua or old('rua')}}" required>
                                </div>
                            </div>

                            <div class="form-group col-sm-6">
                                <label>Número:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <input type="text" class="form-control" id="numero" name="numero" value="{{$clientePjId->numero or old('numero')}}" required>
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="form-group col-sm-6">
                                <label>Email :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                    <input type="email" class="form-control" id="email" name="email" value="{{$clientePjId->email or old('email')}}" required>
                                </div>
                            </div>

                        </div>









                        <div class="row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-block btn-success">Enviar</button>
                            </div>
                            <div class="col-sm-6">
                                <button type="reset" class="btn btn-block btn-warning">Limpar</button>
                            </div>
                        </div>

                    </form>

                </div>

            </div>
        </form>
    </div>
</div>
</form>
</div>
</div>

@endsection