@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Ordens de Serviço
            <small>Encerramento de Ordens de Serviço</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Ordens de Serviço</li>
            <li class="active">Encerramento de Ordens de Serviço</li>
        </ol>   
    </section>    

    <div class="col-sm-12 boxCenterPartida">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif   

        <form action="{{route('empresa.salva.ordem.servico.cliente.pf.finalizada', $ordemServicoPfId->id)}}" method="post">



            {{ csrf_field() }}

            <div class="box box-info boxCriarPartida">


                <div class="box-header with-border">
                    <h3 class="box-title">Encerramento de Ordens de Serviço</h3>
                </div>



                <div id="status"></div>


                <div class="box-body">
                        <div class="row">


                            <div class="col-sm-6">


                <div class="form-group">
                    <label for="cliente_pf_id">Cliente:</label>
                    <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                    <input type="text" class="form-control" value="{{$ordemServicoPfId->cliente_pf->nome_cliente or old('nome_cliente')}}" readonly="readonly">

                </div>


                <label for="data">Funcionário:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                <input type="text" class="form-control" value="{{$ordemServicoPfId->funcionario->nome or old('nome')}}" readonly="readonly">  </div>


                </div>







                        </div>



                        <div class="row">



                            <div class="form-group col-md-6">
                                <label for="hora">Hora:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="text" class="form-control" id="hora" name="hora" value="{{$ordemServicoPfId->hora or old('hora')}}" readonly="readonly">
                                </div>
                           



                                <label for="problema_relatado">Problema Relatado:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-futbol-o"></i>
                                    </div>
                                    <input type="text" class="form-control" id="problema_relatado" name="problema_relatado" value="{{$ordemServicoPfId->problema_relatado or old('problema_relatado')}}" readonly="readonly">
                                </div>
                            </div>
 </div>





                        </div>


                        <div class="row">


                            <div class="col-sm-6">


                <div class="form-group">
                    <label for="valor_total">Valor:</label>
                    <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                    <input type="number" step="0.01" class="form-control" id="valor_total" name="valor_total">

                </div>


                


                </div>







                        </div>



                        <div class="row">



                            <div class="form-group col-md-6">
                                <label for="solucao">Solução:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="text" class="form-control" id="solucao" name="solucao">
                                </div>
                           



                                
                            </div>
 </div>



 

                        </div>



                  

                        <div class="row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-block btn-success">Enviar</button>
                            </div>
                            <div class="col-sm-6">
                                <button type="reset" class="btn btn-block btn-warning">Limpar</button>
                            </div>
                        </div>


                </div>

            </div>
        </form>
    </div>
</div>
</div>
</div>

@endsection