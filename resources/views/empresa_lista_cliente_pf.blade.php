@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Clientes
            <small>Lista de Clientes - Pessoa Física</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Clientes</li>
            <li class="active">Lista de Clientes - Pessoa Física</li>
        </ol>   
    </section>

    <form method="post" action="{{route('empresa.lista.cliente.pf.filtros')}}">
        {{ csrf_field() }}

        <div class="selectSearch">
          

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="cidade_id">Cidade:</label>
                    <select class="form-control" id="cidade_id" name="cidade_id">
                        <option value=""></option>

                        @foreach($cidades as $cidade)    
                        <option value="{{$cidade->id}}">{{$cidade->nome_cidade}}</option>
                        @endforeach    
                    </select>
                </div>
            </div>


            <div class='col-sm-6 btnPesquisar'>
                <button type="submit" class="btn btn-primary btn-outline">Pesquisar</button> 
                <a href="{{route('empresa.cadastro.cliente.pf')}}" class="btn btn-outline btn-success">Cadastrar Novo Cliente</a> 
                           
            </div>
        </div>
    </form>

    <div class='col-sm-12'>

         @if (count($clientes_pf)==0)
        <div class="alert alert-danger">
            Não Existem Clientes Com O Filtro Informado
        </div>
         @endif
    </div>

    <div class="col-sm-12 boxCenter boxCentralize">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Lista de Clientes</h3>
                </div>
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="thListagem">Nome do Cliente</th>
                                <th class="thListagem">Telefone para Contato</th>
                                <th class="thListagem">CPF</th>
                                <th class="thListagem">Estado</th>
                                <th class="thListagem">Cidade</th>
                                <th class="thListagem">CEP</th>
                                <th class="thListagem">Bairro</th>
                                <th class="thListagem">Rua</th>
                                <th class="thListagem">Número</th>

                                <th class="thListagem">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($clientes_pf as $cliente_pf)

                            <tr>

                                <td class="tdListagem">{{$cliente_pf->nome_cliente}}</td>
                                <td class="tdListagem">{{$cliente_pf->telefone}}</td>
                                <td class="tdListagem">{{$cliente_pf->cpf}}</td>
                                <td class="tdListagem">{{$cliente_pf->estado->nome_estado}}</td>
                                <td class="tdListagem">{{$cliente_pf->cidade->nome_cidade}}</td>
                                <td class="tdListagem">{{$cliente_pf->cep}}</td>
                                <td class="tdListagem">{{$cliente_pf->bairro}}</td>
                                <td class="tdListagem">{{$cliente_pf->rua}}</td>
                                <td class="tdListagem">{{$cliente_pf->numero}}</td>

                                <td>
                                   

                                    <a href="{{route('empresa.edita.cliente.pf', $cliente_pf->id)}}" 
                                       class="btn btn-success btn-outline" 
                                       role="button">Editar
                                    </a>

                                    <form style="display: inline-block"
                          method="post"
                          action="{{route('empresa.deleta.cliente.pf', $cliente_pf->id)}}"
                          onsubmit="return confirm('Confirma Exclusão do Cliente Pessoa Física?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger btn-outline"> Excluir </button>
                    </form> 


                                </td>
                                
                                @endforeach
                                
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>  
        </div>
    </div>
</div>
@endsection