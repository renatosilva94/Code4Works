@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Clientes
            <small>Lista de Clientes - Pessoa Juridica</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Clientes</li>
            <li class="active">Lista de Clientes - Pessoa Juridica</li>
        </ol>   
    </section>

    <form method="post" action="{{route('empresa.lista.cliente.pj.filtros')}}">
        {{ csrf_field() }}

        <div class="selectSearch">


            <div class="col-sm-4">
                <div class="form-group">
                    <label for="cidade_id">Cidade:</label>
                    <select class="form-control" id="cidade_id" name="cidade_id">
                        <option value=""></option>

                        @foreach($cidades as $cidade)    
                        <option value="{{$cidade->id}}">{{$cidade->nome_cidade}}</option>
                        @endforeach    
                    </select>
                </div>
            </div>


            <div class='col-sm-6 btnPesquisar'>
                <button type="submit" class="btn btn-primary btn-outline">Pesquisar</button> 
                <a href="{{route('empresa.cadastro.cliente.pj')}}" class="btn btn-outline btn-success">Cadastrar Novo Cliente</a>            
            </div>
        </div>
    </form>

    <div class='col-sm-12'>

        @if (count($clientes_pj)==0)
        <div class="alert alert-danger">
            Não Existem Clientes Com O Filtro Informado
        </div>
        @endif
    </div>

    <div class="col-sm-12 boxCenter boxCentralize">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Lista de Clientes</h3>
                </div>
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="thListagem">Nome Fantasia</th>
                                <th class="thListagem">Razão Social</th>
                                <th class="thListagem">CNPJ</th>
                                <th class="thListagem">Estado</th>
                                <th class="thListagem">Cidade</th>
                                <th class="thListagem">CEP</th>
                                <th class="thListagem">Bairro</th>
                                <th class="thListagem">Rua</th>
                                <th class="thListagem">Número</th>
                                <th class="thListagem">Telefone</th>
                                <th class="thListagem">Email</th>



                                <th class="thListagem">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($clientes_pj as $cliente_pj)

                            <tr>

                                <td class="tdListagem">{{$cliente_pj->nome_fantasia}}</td>
                                <td class="tdListagem">{{$cliente_pj->razao_social}}</td>
                                <td class="tdListagem">{{$cliente_pj->cnpj}}</td>
                                <td class="tdListagem">{{$cliente_pj->estado->nome_estado}}</td>
                                <td class="tdListagem">{{$cliente_pj->cidade->nome_cidade}}</td>
                                <td class="tdListagem">{{$cliente_pj->cep}}</td>
                                <td class="tdListagem">{{$cliente_pj->bairro}}</td>
                                <td class="tdListagem">{{$cliente_pj->rua}}</td>
                                <td class="tdListagem">{{$cliente_pj->numero}}</td>
                                <td class="tdListagem">{{$cliente_pj->telefone}}</td>
                                <td class="tdListagem">{{$cliente_pj->email}}</td>


                                <td>
                                    

                                    <a href="{{route('empresa.edita.cliente.pj', $cliente_pj->id)}}" 
                                       class="btn btn-success btn-outline" 
                                       role="button">Editar
                                    </a>

                                    <form style="display: inline-block"
                          method="post"
                          action="{{route('empresa.deleta.cliente.pj', $cliente_pj->id)}}"
                          onsubmit="return confirm('Confirma Exclusão do Cliente Pessoa Juridica?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger btn-outline"> Excluir </button>
                    </form> 


                                </td>

                                @endforeach

                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>  
        </div>
    </div>
</div>
@endsection