@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Recursos Humanos
            <small>Lista de Cargos e Salários</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Recursos Humanos</li>
            <li class="active">Lista de Cargos e Salários</li>
        </ol>   
    </section>

    <form method="post" action="#">
        {{ csrf_field() }}

        <div class="selectSearch">
          

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="cidade_id">Cidade:</label>
                    <select class="form-control" id="cidade_id" name="cidade_id">
                        <option value=""></option>

                        @foreach($cidades as $cidade)    
                        <option value="{{$cidade->id}}">{{$cidade->nome_cidade}}</option>
                        @endforeach    
                    </select>
                </div>
            </div>


            <div class='col-sm-1 btnPesquisar'>
                <button type="submit" class="btn btn-bitbucket">Pesquisar</button>            
            </div>
            <a href="{{route('empresa.cadastro.cargo.salario')}}" class="btn btn-danger">Cadastrar Cargo e Salário</a>
        </div>
    </form>

    <div class='col-sm-12'>

         @if (count($cargossalarios)==0)
        <div class="alert alert-danger">
            Não Existem Cargos e Salários Com O Filtro Informado
        </div>
         @endif
    </div>

    <div class="col-sm-12 boxCenter boxCentralize">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Lista de Cargos e Salários</h3>
                </div>
                <div class="box-body">

                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="thListagem">Código</th>
                                <th class="thListagem">Nome do Cargo</th>
                                <th class="thListagem">Salário</th>

                                <th class="thListagem">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cargossalarios as $cargosalario)

                            <tr>

                                <td class="tdListagem">{{$cargosalario->id}}</td>
                                <td class="tdListagem">{{$cargosalario->nome_cargo}}</td>
                                <td class="tdListagem">{{$cargosalario->salario}}</td>
                                
                                


                                <td>
                                   

                                    <a href="#" 
                                       class="btn btn-success" 
                                       role="button">Editar
                                    </a>

                                    <form style="display: inline-block"
                          method="post"
                          action="#"
                          onsubmit="return confirm('Confirma Exclusão do Cargo ?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger"> Excluir </button>
                    </form> 


                                </td>
                                
                                @endforeach
                                
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>  
        </div>
    </div>
</div>
@endsection