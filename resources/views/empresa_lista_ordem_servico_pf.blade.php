@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Ordens de Serviço
            <small>Lista de Ordens de Serviço - Pessoa Física</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Ordens de Serviço</li>
            <li class="active">Lista de Ordens de Serviço - Pessoa Física</li>
        </ol>   
    </section>

    <form method="post" action="#">
        {{ csrf_field() }}

        <div class="selectSearch">


            <div class="col-sm-4">
                <div class="form-group">
                    <label for="cidade_id">Cidade:</label>
                    <select class="form-control" id="cidade_id" name="cidade_id">
                        <option value=""></option>

                        @foreach($cidades as $cidade)    
                        <option value="{{$cidade->id}}">{{$cidade->nome_cidade}}</option>
                        @endforeach    
                    </select>
                </div>
            </div>


            <div class='col-sm-6 btnPesquisar'>
                <button type="submit" class="btn btn-primary btn-outline">Pesquisar</button> 
                                <a href="{{route('empresa.cadastro.ordem.servico.pf')}}" class="btn btn-outline btn-success">Nova Ordem de Serviço</a>            
           
            </div>
        </div>
    </form>

    <div class='col-sm-12'>

        @if (count($ordens_servico_pf)==0)
        <div class="alert alert-danger">
            Não Existem Ordens de Serviço Com O Filtro Informado
        </div>
        @endif
    </div>

    <div class="col-sm-12 boxCenter boxCentralize">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Lista de Ordens de Serviço - Pessoa Física</h3>
                </div>
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="thListagem">Código</th>
                                <th class="thListagem">Nome do Cliente</th>
                                <th class="thListagem">Funcionário Responsável</th>
                                <th class="thListagem">Data</th>
                                <th class="thListagem">Hora</th>

                                <th class="thListagem">Valor Total</th>
                                <th class="thListagem">Solução</th>
                                
                                <th class="thListagem">Status</th>
                                <th class="thListagem">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ordens_servico_pf as $ordem_servico_pf)

                            <tr>

                                <td class="tdListagem">{{$ordem_servico_pf->id}}</td>
                                <td class="tdListagem">{{$ordem_servico_pf->cliente->nome_cliente}}</td>
                                <td class="tdListagem">{{$ordem_servico_pf->funcionario->nome}}</td>
                                <td class="tdListagem">{{$ordem_servico_pf->data}}</td>
                                <td class="tdListagem">{{$ordem_servico_pf->hora}}</td>
                                <td class="tdListagem">{{$ordem_servico_pf->valor_total}}</td>
                                <td class="tdListagem">{{$ordem_servico_pf->solucao}}</td>


                                @if($ordem_servico_pf->status == 0)
                                <td class="tdListagem"><button class="btn btn-danger btn-sm">Em Aberto</button></td>
                                @else
                                <td class="tdListagem"><button class="btn btn-success btn-sm">Finalizada</button></td>
                                @endif
                                


                                <td>
                                    

                                    

                                    <form style="display: inline-block"
                          method="post"
                          action="{{route('empresa.deleta.ordem.servico.cliente.pf', $ordem_servico_pf->id)}}"
                          onsubmit="return confirm('Confirma Exclusão da Ordem de Serviço?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger btn-sm"> Excluir </button>
                    </form> 

                                    <a href="{{route('empresa.gerar.pdf.ordem.servico.pf', $ordem_servico_pf->id)}}" 
                                       class="btn btn-info btn-sm" 
                                       role="button">PDF
                                    </a>

                                @if($ordem_servico_pf->status == 0)

                                    <a href="{{route('empresa.finaliza.ordem.servico.pf', $ordem_servico_pf->id)}}" 
                                       class="btn btn-warning btn-sm" 
                                       role="button">Finalizar
                                    </a>
                                    @else


                                @endif


















                                </td>

                                @endforeach

                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>  
        </div>
    </div>
</div>
@endsection