@extends('empresa_dashboard')

@section('conteudo')

<!-- MODAL -->
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Carregar Arquivo XML</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        </button>
      </div>
      <div class="modal-body">
        <input type="file">
      </div>
      <div class="modal-footer">
      <form action="#" method="post" enctype="multipart/form-data">
        
        <button type="submit" class="btn btn-primary btn-sm">Enviar</button>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Fechar</button>
        </form>

      </div>
    </div>
  </div>
</div>


















<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Compras
            <small>Cadastro de Compras</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Compras</li>
            <li class="active">Cadastro de Compras</li>
        </ol>   
    </section>    

    <div class="col-sm-12 boxCenterPartida">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif   

<div id="formulario">

        <form action="{{route('empresa.cadastra.compra')}}" method="post">



            {{ csrf_field() }}

            <div class="box box-info boxCriarPartida">


                <div class="box-header with-border">
                    <h3 class="box-title">Cadastro de Compras</h3>
                </div>


                <div id="status"></div>


                <div class="box-body">
                        

                        <div id="sections">
  <div class="section">

    <div class="row">

                <div class="form-group col-md-12">
            <fieldset>
                                <label for="numero_nota_fiscal">Número da Nota Fiscal:</label> 

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="text" class="form-control" id="numero_nota_fiscal" name="numero_nota_fiscal"  required>
                                </div>
                            </div>


                            <div class="form-group col-sm-12">
                                    <label for="produto_id">Produto:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-map-signs"></i>
                                        </div>
                                        <select class="form-control" id="produto_id" name="produto_id">
                                            <option></option>
                                            @foreach($produtos as $produto)
                                            <option value="{{$produto->id}}">{{$produto->nome_produto}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>





                            <div class="form-group col-md-12">
                                <label for="quantidade_embalagens">Quantidade de Embalagens:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="number" class="form-control" id="quantidade_embalagens" name="quantidade_embalagens" required>
                                </div>
                            </div>



                            <div class="form-group col-md-6">
                                <label for="preco_embalagem">Preço da Embalagem:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-futbol-o"></i>
                                    </div>
                                    <input type="int" step="0.01" class="form-control" id="preco_embalagem" name="preco_embalagem" required>
                                </div>
                            </div>








                            <div class="form-group col-md-6">
                                <label for="quantidade_produtos_embalagem">Quantidade de Produtos na Embalagem:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-commenting"></i>
                                    </div>
                                    <input type="number" class="form-control" id="quantidade_produtos_embalagem" name="quantidade_produtos_embalagem" required>
                                </div>
                            </div>



                            <div class="form-group col-md-6">
                                <label for="preco_unitario">Preço Unitário:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-futbol-o"></i>
                                    </div>
                                    <input type="number" step="0.01" class="form-control" id="preco_unitario" name="preco_unitario" required>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="preco_total">Preço Total:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-futbol-o"></i>
                                    </div>
                                    <input type="number" step="0.01" class="form-control" id="preco_total" name="preco_total" required>
                                </div>
                            </div>


<script>
        
        /*Calculo da Quantidade do Produto em Estoque*/

        $('#produto_id').on('change', function () {
                                    var produtoID = $(this).val();
                                    if (produtoID) {
                                        $.ajax({
                                            type: "GET",
                                            url: "{{url('ajax/pegar-quantidade-produtos')}}?id=" + produtoID,
                                            success: function (res) {
                                                if(res) {
                                                    
                                                    $("#quantidadeEstoqueAtual").empty();
                                                    $.each(res, function (key, value) {
                                                      //  $("#quantidadeEstoque").append('<option value="' + key + '">' + value + '</option>');

                                                        $("#quantidadeEstoqueAtual").val(value);
                                                    });

                                                } else {

                                                    $("#quantidadeEstoqueAtual").empty();

                                                }
                                            }
                                        });
                                    } else {
                                        $("#quantidadeEstoqueAtual").empty();
                                    }

                                });
                                

        /*Calculo da Quantidade de Produto Posterior em Estoque*/

        var quantidade_produtos_embalagem = document.getElementById('quantidade_produtos_embalagem');
         var quantidade_embalagens = document.getElementById('quantidade_embalagens');
        var preco_embalagem = document.getElementById('preco_embalagem');



//Calculo de Quantidade Posterior
        $('#quantidade_produtos_embalagem').on('focusout', function () {
            var soma_estoque = parseInt(quantidade_produtos_embalagem.value) + parseInt(quantidadeEstoqueAtual.value);
            quantidadeEstoquePosterior.value = soma_estoque;
        });


//Calculo do Preço Total
        $('#preco_embalagem').on('focusout', function () {
            var preco_total_resultado = (quantidade_embalagens.value) * parseFloat(preco_embalagem.value);
            preco_total.value = (parseFloat(preco_total_resultado).toFixed(2));
        });

        //Calculo de Preço Unitário

        $('#quantidade_produtos_embalagem').on('focusout', function () {
            var preco_unitario_resultado = (parseFloat(preco_embalagem.value) / (quantidade_produtos_embalagem.value)) ;
            preco_unitario.value = (parseFloat(preco_unitario_resultado).toFixed(2));
        });



        
        </script>


                            <div class="form-group col-md-6">
            <label for="quantidadeEstoqueAtual">Quantidade do Produto em Estoque:</label>
            <input type="number" class="form-control" id="quantidadeEstoqueAtual"
                   name="quantidadeEstoqueAtual"
                   required readonly="readonly">
        </div>

                            <div class="form-group col-md-6">
            <label for="quantidadeEstoquePosterior">Quantidade Posterior :</label>
            <input type="number" class="form-control" id="quantidadeEstoquePosterior"
                   name="quantidadeEstoquePosterior"
                   required readonly="readonly">
        </div>

        </div>


                  

                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-block btn-success btn-sm">Enviar</button>
                            </div>
                            <div class="col-sm-2">
                                <button type="reset" class="btn btn-block btn-warning btn-sm">Limpar</button>
                            </div>
</form>
                           
                            
                            <!-- Botão para Remover Formulário 
                            <div class="col-sm-2">
                                <button type="" class="remove btn btn-block btn-info btn-sm">Remover Formulário</button>
                            </div>
-->
                            
                            
    </fieldset>

<!-- Botão para Adicionar Formulário
  <div class="col-sm-2">
                                <button type="" class="addsection btn btn-block btn-danger btn-sm">Adicionar Formulário</button>
                            </div>-->
                    
  
 <div class="col-sm-2">
                                <a href="#" class="btn btn-block btn-yahoo btn-sm" title="Ao Carregar o XML ele é salvo no servidor e todos os produtos presente na nota são abastecidos" data-toggle="modal" data-target="#exampleModal">Carregar XML</a>
                            </div>
                </div>

            </div>
    </div>

    </div>
</div>

<!--
<script>
    


    //define template
var template = $('#sections .section:first').clone();

//define counter
var sectionsCount = 1;


//add new section
$('body').on('click', '.addsection', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

        //set id to store the updated section number
        var newId = this.id + sectionsCount;

        //update for label
        $(this).prev().attr('for', newId);

        //update id
        this.id = newId;

    }).end()

    //inject new section
    .appendTo('#sections');
    return false;
});

//remove section
$('#sections').on('click', '.remove', function() {
    //fade out section
    $(this).parent().fadeOut(300, function(){
        //remove parent element (main section)
        $(this).parent().parent().empty();
        return false;
    });
    return false;
});
</script>
-->


</div>
</div>
</div>

@endsection