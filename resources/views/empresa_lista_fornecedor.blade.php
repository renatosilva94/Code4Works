@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Fornecedores
            <small>Lista de Fornecedores</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Fornecedores</li>
            <li class="active">Lista de Fornecedores</li>
        </ol>   
    </section>

    <form method="post" action="#">
        {{ csrf_field() }}

        <div class="selectSearch">


            <div class="col-sm-4">
                <div class="form-group">
                    <label for="cidade_id">Cidade:</label>
                    <select class="form-control" id="cidade_id" name="cidade_id">
                        <option value=""></option>

                        @foreach($cidades as $cidade)    
                        <option value="{{$cidade->id}}">{{$cidade->nome_cidade}}</option>
                        @endforeach    
                    </select>
                </div>
            </div>


            <div class='col-sm-6 btnPesquisar'>
                <button type="submit" class="btn btn-primary btn-outline">Pesquisar</button> 
                <a href="{{route('empresa.cadastro.fornecedor')}}" class="btn btn-outline btn-success">Cadastrar Novo Fornecedor</a>            
            </div>
        </div>
    </form>

    <div class='col-sm-12'>

        @if (count($fornecedores)==0)
        <div class="alert alert-danger">
            Não Existem Fornecedores Com O Filtro Informado
        </div>
        @endif
    </div>

    <div class="col-sm-12 boxCenter boxCentralize">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Lista de Clientes</h3>
                </div>
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="thListagem">Nome Fantasia</th>
                                <th class="thListagem">Razão Social</th>
                                <th class="thListagem">CNPJ</th>
                                <th class="thListagem">Estado</th>
                                <th class="thListagem">Cidade</th>
                                <th class="thListagem">CEP</th>
                                <th class="thListagem">Bairro</th>
                                <th class="thListagem">Rua</th>
                                <th class="thListagem">Número</th>
                                <th class="thListagem">Telefone</th>
                                <th class="thListagem">Email</th>



                                <th class="thListagem">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($fornecedores as $fornecedor)

                            <tr>

                                <td class="tdListagem">{{$fornecedor->nome_fantasia}}</td>
                                <td class="tdListagem">{{$fornecedor->razao_social}}</td>
                                <td class="tdListagem">{{$fornecedor->cnpj}}</td>
                                <td class="tdListagem">{{$fornecedor->estado->nome_estado}}</td>
                                <td class="tdListagem">{{$fornecedor->cidade->nome_cidade}}</td>
                                <td class="tdListagem">{{$fornecedor->cep}}</td>
                                <td class="tdListagem">{{$fornecedor->bairro}}</td>
                                <td class="tdListagem">{{$fornecedor->rua}}</td>
                                <td class="tdListagem">{{$fornecedor->numero}}</td>
                                <td class="tdListagem">{{$fornecedor->telefone}}</td>
                                <td class="tdListagem">{{$fornecedor->email}}</td>


                                <td>
                                    

                                    <a href="#" 
                                       class="btn btn-success btn-outline" 
                                       role="button">Editar
                                    </a>

                                    <a href="#" 
                                       class="btn btn-danger btn-outline" 
                                       role="button">Excluir
                                    </a>


                                </td>

                                @endforeach

                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>  
        </div>
    </div>
</div>
@endsection