<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Code4Works - Cadastro</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="{{ asset('css/empresa_cadastro.css') }}">
        <link rel="icon" href="https://image.ibb.co/d5ZQFw/23336562_1485648751515482_1712329585_o.png" />
   
        <script src='https://www.google.com/recaptcha/api.js'></script>

    <script>
    window.onload = function() {
    var recaptcha = document.forms["form-cadastro"]["g-recaptcha-response"];
    recaptcha.required = true;
    recaptcha.oninvalid = function(e) {
    alert("Por Favor, Complete o Captcha");
      }
   }
   </script>
   
    </head>
    <body>
        <div class="container" >  
            <div class="col-lg-4 col-md-3 col-sm-2"></div>
            <div class="col-lg-4 col-md-6 col-sm-8">

               <!-- Inicio da Modal de Termos de Uso -->



<div class="modal" id="modal-termos-uso" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Termos de Uso</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>1 - Ao se cadastrar você se tornara um usuário Beta e estará sujeiro a aprovação do desenvolvedor do sistema.</p>
        <br>
        <p>2 - Você pode mas não é obrigado a fornecer estatiscas de uso do software, lembre-se que isso colabora no desenvolvimento do mesmo.</p>
        <br>
        <p>3 - O Software sofrerá alterações constantes durante o seu tempo de desenvolvimento.</p>
        <br>
        <p>4 - No momento o software é gratuito para todos os testadores Beta.</p>
        <br>
        <p>5 - Evite bloqueadores de anuncio.</p>
        <br>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>





               <!-- Fim da Modal -->


                <div class="row loginbox">                    
                    <div class="col-sm-12 boxRodapeCadastro">
                        <center><img class="loginAvatar" src="/img/avatar.png"></center>
                            
                    </div>
                    
                    <center><span class="singtext" >Cadastro - Code4Works</span>  </center>
                    
                    <div class="boxFormCadastro">
                        <form action="{{ route('empresa.cadastro.efetuar') }}" method="post" id="form-cadastro">

                            {{ csrf_field() }}



                            <div class="form-group">
                                <label for="nome">Nome: </label>
                                <input type="text" class="form-control" id="nome" name="nome" required>
                            </div>

                            <div class="form-group">
                                <label for="sobrenome">Sobrenome: </label>
                                <input type="text" class="form-control" id="sobrenome" name="sobrenome" required>
                            </div>

                            <div class="form-group">
                                <label for="email">Email: </label>
                                <input type="text" class="form-control" id="email" name="email" required>
                            </div>

                            <div class="form-group">
                                <label for="senha">Senha: </label>
                                <input type="password" class="form-control" id="password" name="password" required>
                            </div>
                            
                            <div class="form-group">
                                <label for="telefone">Telefone: </label>
                                <input type="number" class="form-control" id="telefone" name="telefone" placeholder="(XX)XXXXXXXXX" required>
                            </div>

                            <div class="form-group">
                                <label for="sexo">Sexo:</label>
                                <select class="form-control" id="sexo" name="sexo">

                                    <option value="M"
                                            @if ((isset($reg) and $reg->sexo == "Masculino") or old('sexo') == "M") selected @endif                        
                                            >Masculino</option>
                                    <option value="F"
                                            @if ((isset($reg) and $reg->sexo == "Feminino") or old('sexo') == "F") selected @endif                                                
                                            >Feminino</option>
                                    <option value="F"
                                            @if ((isset($reg) and $reg->sexo == "Outro") or old('sexo') == "O") selected @endif                                                
                                            >Outro</option>
                                    
                                </select>
                            </div>

                                <div class="g-recaptcha" data-sitekey="6LfKwmIUAAAAANrcmgDT64APiDYdCxwfs_0qh68K"></div>


                            
                                <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="termos_uso" onchange="document.getElementById('enviar').disabled = !this.checked;">
                            <a href="#termos_uso" data-toggle="modal" data-target="#modal-termos-uso">Li e Aceito Os Termos de Uso</a>
                                </div>
                           
                           <br>


                            <center><button type="submit" class="btn btn-primary" id="enviar" disabled="disabled">Enviar</button>
                                <button type="reset" class="btn btn-warning" id="limpar">Limpar</button></center>

                        </form>
                    </div>

                </div>
                <br>                
                <br>
            </div>
            <div class="col-lg-4 col-md-3 col-sm-2"></div>
        </div>
    </body>
</html>

