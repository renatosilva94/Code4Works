@extends('empresa_dashboard')

@section('conteudo')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Funcionarios
            <small>Lista de Funcionarios</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="">Funcionarios</li>
            <li class="active">Lista de Funcionarios</li>
        </ol>   
    </section>

    <form method="post" action="#">
        {{ csrf_field() }}

        <div class="selectSearch">
          

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="cidade_id">Cidade:</label>
                    <select class="form-control" id="cidade_id" name="cidade_id">
                        <option value=""></option>

                        @foreach($cidades as $cidade)    
                        <option value="{{$cidade->id}}">{{$cidade->nome_cidade}}</option>
                        @endforeach    
                    </select>
                </div>
            </div>


            <div class='col-sm-6 btnPesquisar'>
                <button type="submit" class="btn btn-ouline btn-primary">Pesquisar</button>  
                <a href="{{route('empresa.cadastro.funcionario')}}" class="btn btn-success btn-ouline">Novo Funcionário</a>        
            </div>
        </div>
    </form>

    <div class='col-sm-12'>

         @if (count($funcionarios)==0)
        <div class="alert alert-danger">
            Não Existem Funcionarios Com O Filtro Informado
        </div>
         @endif
    </div>

    <div class="col-sm-12 boxCenter boxCentralize">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Lista de Funcionarios</h3>
                </div>
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="thListagem">Nome</th>
                                <th class="thListagem">Sobrenome</th>
                                <th class="thListagem">RG</th>
                                <th class="thListagem">CPF</th>
                                <th class="thListagem">CEP</th>
                                <th class="thListagem">Bairro</th>
                                <th class="thListagem">Rua</th>
                                <th class="thListagem">Número</th>
                                <th class="thListagem">Pis/Pasep</th>
                                <th class="thListagem">Cargo</th>
                                <th class="thListagem">Email</th>
                                <th class="thListagem">Empresa</th>




                                <th class="thListagem">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($funcionarios as $funcionario)

                            <tr>

                                <td class="tdListagem">{{$funcionario->nome}}</td>
                                <td class="tdListagem">{{$funcionario->sobrenome}}</td>
                                <td class="tdListagem">{{$funcionario->rg}}</td>
                                <td class="tdListagem">{{$funcionario->cpf}}</td>
                                <td class="tdListagem">{{$funcionario->cep}}</td>
                                <td class="tdListagem">{{$funcionario->bairro}}</td>
                                <td class="tdListagem">{{$funcionario->rua}}</td>

                                <td class="tdListagem">{{$funcionario->numero}}</td>
                                <td class="tdListagem">{{$funcionario->pispasep}}</td>
                                <td class="tdListagem">{{$funcionario->cargo_salario->nome_cargo}}</td>
                                <td class="tdListagem">{{$funcionario->email}}</td>
                                                                <td class="tdListagem">{{$funcionario->usuario->nome}} {{$funcionario->usuario->sobrenome}}</td>



                                <td>
                                   

                                    <a href="{{route('empresa.edita.funcionario', $funcionario->id)}}" 
                                       class="btn btn-ouline btn-success " 
                                       role="button">Editar
                                    </a>

                                    <form style="display: inline-block"
                          method="post"
                          action="#"
                          onsubmit="return confirm('Confirma Exclusão do Funcionário ?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-ouline btn-danger "> Excluir </button>
                    </form> 


                                </td>
                                
                                @endforeach
                                
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>  
        </div>
    </div>
</div>
@endsection